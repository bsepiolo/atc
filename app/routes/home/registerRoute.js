/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var userModel = require('../../models/userModel');


router.get('/api/register', function (req, res) {
    //  res.sendfile('./public/views/home/shared/landingPageLayout.html'); // load our public/loginCustomView.html file

})

router.post('/api/register', function (req, res) {
    var item = {
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        gender: req.body.gender,
        date: req.body.date,
        password: bcrypt.hashSync(req.body.password, 8)
    }
    var data = new userModel(item);

    userModel.findOne({email: req.body.email}, function (err, result) {

        if (err)
            res.send(err);
        if (!result) {
            data.save(function(err,result){
                if(err)
                    console.log(err)
              //  res.json(result);
                passport.authenticate('local-login')(req, res, function () {

                    res.status(200).json({
                        message: 'registration successful',
                        result: result
                    });
                })
            });

        } else {
            res.status(409).json({
                message: 'user already exists'
            });
        }
        // res.json(user);

        //      console.log("zxczxc",user); // return all nerds in JSON format
    });

})


module.exports = router;