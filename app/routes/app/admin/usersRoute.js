/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');

var express = require('express');
var _ = require('underscore');
var router = express.Router();
var userModel = require('../../../models/userModel');

require('../../../../config/passport')(passport); // pass passport for configuration


router.get('/api/admin/users', isLoggedIn, function (req, res, next) {

    userModel.find({ _id: { $ne: req.user._id }}).exec(function (err, result) {
        res.json(result)
    })
});
router.delete('/api/admin/users/:id', isLoggedIn, function (req, res, next) {

    userModel.findOne({_id: req.params.id}).remove().exec(function(err,result){
        userModel.find({},function(err,result){
            res.json(result)

        })
    })
    // userModel.findOne({_id: req.user._id}, {'following.user': req.params.id}, function (err, result) {
    //     result.following.user(req.user._id).remove();
    //     result.save();
    // })


    // userModel.findOne({_id: req.user._id}, {following: {$elemMatch: {'user': req.params.id}}}).remove().exec();
})
router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated() && req.user.role==="Administrator") {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;