/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');

var express = require('express');
var _ = require('underscore');
var router = express.Router();
var userModel = require('../../../models/userModel');
var dictionariesModel = require('../../../models/dictionariesModel');

require('../../../../config/passport')(passport); // pass passport for configuration


router.get('/api/admin/dictionaries', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/hobby', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0].hobbies.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/job', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0].jobs.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/university', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0].universityCourses.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/eyes', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0]._doc.eyesColors.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/figure', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0]._doc.figure.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/hair', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0]._doc.hair.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.post('/api/admin/dictionaries/education', isLoggedIn, function (req, res, next) {

    dictionariesModel.find({}).exec(function (err, result) {
        result[0]._doc.educationLevel.push({name: req.body.name})
        result[0].save();
        res.json(result)
    })
});
router.delete('/api/admin/dictionaries/:id', isLoggedIn, function (req, res, next) {

    dictionariesModel.findOne({_id: req.params.id}).remove().exec(function(err,result){
        dictionariesModel.find({},function(err,result){
            res.json(result)

        })
    })
    // userModel.findOne({_id: req.user._id}, {'following.user': req.params.id}, function (err, result) {
    //     result.following.user(req.user._id).remove();
    //     result.save();
    // })


    // userModel.findOne({_id: req.user._id}, {following: {$elemMatch: {'user': req.params.id}}}).remove().exec();
})

router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated() && req.user.role==="Administrator") {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;