/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');

var express = require('express');
var request = require('request');
var _ = require('underscore');
var async = require('async');
var router = express.Router();
var distance = require('google-distance');
var geolib = require('geolib')
distance.apiKey = 'AIzaSyA4FufWjJ7wpq6fllBqLULYf9MNwaB1eGE';
var userModel = require('../../models/userModel');

require('../../../config/passport')(passport); // pass passport for configuration
var data = {};
var urls = [];
router.get('/api/suggestions', isLoggedIn, function (req, res, next) {
    var users = [];
    var j=0;
    var genderToFind;
    var currentUserCoords = [];
    if (req.user.gender == 'male') {
        genderToFind = 'female'
    } else {
        genderToFind = 'male'
    }
    userModel.findOne({_id: req.user._id}, function (err, result) {
        if (err)
            res.send(err);
        currentUserCoords.longitude = result.placeOfResidence.longitude;
        currentUserCoords.latitude = result.placeOfResidence.latitude;
    });
    var i;

    userModel.find({gender: genderToFind}, function (err, result) {


        for (i = 0; i < result.length; i++) {
            var currentPoints = 0;
            console.log(result[i].firstName)
            if (result[i].gallery.length > 2) {
                currentPoints += 2;
            } else if (result[i].gallery.length > 0) {
                currentPoints += 1
            }
            var alienDate = new Date(result[i].date);
            var myDate = new Date(req.user.date);
            var ageDifference;
            var alienYear = alienDate.getFullYear();
            var myYear = myDate.getFullYear();
            if(alienYear>=myYear){
                ageDifference =  alienYear - myYear;
            }else{
                ageDifference = myYear - alienYear;

            }
            if(ageDifference<2){
                currentPoints+=3;
            }else if(ageDifference<3){
                currentPoints+=2;
            }else if(ageDifference<5){
                currentPoints+=1;
            }else if(ageDifference<7){
                currentPoints+=0.5;
            }
            if(result[i].schoolType==req.user.schoolType){
                currentPoints+=1;
            }
            if(result[i].placeOfResidence.latitude.length>0&&currentUserCoords.latitude.length>0){

                var userDistance = geolib.getDistanceSimple(
                    {latitude: result[i].placeOfResidence.latitude, longitude: result[i].placeOfResidence.longitude},
                    {latitude: currentUserCoords.latitude, longitude: currentUserCoords.longitude}
                );
                if (Math.round(userDistance / 1000) <= 50) {
                    // currentPoints += 1;
                    currentPoints += 2;
                } else if (Math.round(userDistance / 1000) <= 100) {
                    // currentPoints += 0.5;
                    currentPoints += 1;
                    // return distanceBetween = Math.round(element.distance.value) / 1000;
                }
            }
            for (var k = 0; k < result[i]._doc.hobbies.length; k++) {
                for (var j = 0; j < req.user._doc.hobbies.length; j++) {
                    if (req.user._doc.hobbies[j] == result[i]._doc.hobbies[k]) {
                        currentPoints += 0.5;
                    }

                }

            }
            result[i]._doc.currentPoints= currentPoints;


            //                      res.json(suggestedUsers.reverse())
            // for(var i=0; i<result._doc.hobbies.length; i++){
            //     for(var k=0; k<req.user._doc.hobbies.length;k++){
            //         if(req.user._doc.hobbies[k]==result._doc.hobbies[i]){
            //             currentPoints+=0.5;
            //         }
            //
            //     }
            //
            // }
            var distanceBetween;
            urls[i] = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + result[i].placeOfResidence.latitude + ',' + result[i].placeOfResidence.longitude + '&destinations=' + currentUserCoords.latitude + ',' + currentUserCoords.longitude + '&key=AIzaSyA4FufWjJ7wpq6fllBqLULYf9MNwaB1eGE';
            result[i]._doc.currentPoints= currentPoints;
            // distance.get(
            //      {
            //          origin: '' + result[i].placeOfResidence.latitude + ',' + result[i].placeOfResidence.longitude,
            //          destination: '' + currentUserCoords.latitude + ',' + currentUserCoords.longitude
            //      },function (err, data, callback) {
            //          if (err){ return console.log(err)}
            //          else{
            //              console.log("currentPointsAfter", Math.round(data.distanceValue))
            //              //if (Math.round(data.distanceValue / 1000) <= 50) {
            //              //    currentPoints += 1;
            //              //} else if (Math.round(data.distanceValue / 1000) <= 100){
            //                 // currentPoints += 0.5;
            //              return distanceBetween = Math.round(data.distanceValue)/1000;
            //             // }
            //          }
            //          //return currentPoints += 7;
            //
            //      });

        }
        var suggestedUsers = _.sortBy(result, function(o) { return o._doc.currentPoints; })
        res.json(suggestedUsers.reverse())
       // urls.map(function (item, callback) {
       //      request(item, function (error, response, body) {
       //          if (!error && response.statusCode == 200) {
       //
       //
       //
       //
       //              var element = JSON.parse(body).rows[0].elements[0];
       //             // console.log("heeeeej",users[j])
       //            //  console.log("BBBBBBBBB",element)
       //              if (element.hasOwnProperty('distance')) {
       //
       //                  console.log("AAAAAAAAAAAAAAAAA",element.distance.value)
       //                  if (Math.round(element.distance.value / 1000) <= 50) {
       //                     // currentPoints += 1;
       //                      result[j]._doc.currentPoints +=2;
       //                  } else if (Math.round(element.distance.value / 1000) <= 100) {
       //                     // currentPoints += 0.5;
       //                      result[j]._doc.currentPoints +=1;
       //                      // return distanceBetween = Math.round(element.distance.value) / 1000;
       //                  }
       //              }
       //                  j++;
       //                  if (j === urls.length) {
       //                      // All download done, process responses array
       //                      console.log("punktacja", result)
       //                      var suggestedUsers = _.sortBy(result, function(o) { return o._doc.currentPoints; })
       //                      res.json(suggestedUsers.reverse())
       //                  }
       //
       //
       //
       //              // Print the google web page.
       //
       //          }
       //      })
       //  })
    })

});
router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;