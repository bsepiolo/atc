/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');
var fs = require('fs');
var express = require('express');
var multer = require('multer');
var Jimp = require('jimp');
var router = express.Router();


var userModel = require('../../../models/userModel');

var data = {};
var item = {}
//var item = {};
var userId;
var fileData;
var imgData = {}
var currentName;
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        fileData = file;

        debugger;
        var uploadPath = './uploads/' + userId;
        fs.exists(uploadPath, function (exists) {
            if (exists) {
                cb(null, uploadPath)
            }
            else {
                fs.mkdir(uploadPath, function (err) {
                    if (err) {
                        console.log('Error in folder creation');
                        next();
                    }
                    cb(null, uploadPath)
                })
            }
        })

    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();

        currentName = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];

        item = {
            originalName: file.originalname,
            currentName: currentName
        }

        cb(null, currentName)
    }
});
var upload = multer({ //multer settings
    storage: storage
}).single('file');
/** API path that will uploads the files */
// router.post('/api/userProfile/:id/files', function(req, res) {
//
// });

router.post('/api/userProfile/:id/files', isLoggedIn, function (req, res, next) {
    debugger;

    upload(req, res, function (err) {
        if (err) {
            res.json({error_code: 1, err_desc: err});
            return;
        } else {

            userModel.findByIdAndUpdate(userId, {
                $push: {gallery: item}
            }, {safe: true, upsert: true, new: true}, function (err, model) {
                console.log(err);
                res.json({"wiruj": "sdf"})
            });
        }
        Jimp.read("./uploads/" + userId + "/" + currentName, function (err, image) {
            if (err) throw err;
            image.resize(Jimp.AUTO, 250)
                .quality(85)// resize
                .write("./uploads/" + userId + "/thumb-" + currentName); // save
        });
    })

    var data = new userModel(imgData);

})
router.post('/api/userProfile/:id', isLoggedIn, function (req, res, next) {

    var personalData = {
        date: req.body.date,
        placeOfBirth: req.body.placeOfBirth,
        placeOfResidence: req.body.placeOfResidence,
        height: req.body.height,
        hair: req.body.hair,
        eyes: req.body.eyes,
        figure: req.body.figure,
        educationLevel: req.body.educationLevel,
        schoolType: req.body.schoolType,
        dateOfCompletion: req.body.dateOfCompletion
    }

    // var data = new userModel(item);
    userModel.findOneAndUpdate({_id: req.params.id}, personalData, {upsert: true}, function (err, result) {

        if (err)
            res.send(err);
        res.json(result)

    })
    console.log(req.body)
})
router.get('/api/userProfile/:id/gallery/', isLoggedIn, function (req, res, next) {
    userId = req.params.id;

    userModel.findOne({_id: req.params.id}, function (err, result) {
        if (err)
            res.send(err);
        res.json({
            result: result,
            userId: req.user._id
        })

    });
});

router.post('/api/userProfile/:id/gallery/avatar/:image', isLoggedIn, function (req, res, next) {

    if (req.user._id == req.params.id) {
        userModel.findOne({_id: req.params.id}, {gallery: {$elemMatch: {'_id': req.params.image}}}).populate("gallery.comments.author").exec(function (err, result) {
            if (err)
                res.send(err);

            var personalData = {
                avatar: result.gallery[0]
            }
            userModel.findOneAndUpdate({_id: req.params.id}, personalData, {upsert: true}, function (err, result) {

                if (err)
                    res.send(err);
                res.json(result)

            })

        });
        // var data = new userModel(item);

        console.log(req.body)
    }
})


router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;