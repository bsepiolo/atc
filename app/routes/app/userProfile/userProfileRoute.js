/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');
var fs = require('fs');
var express = require('express');
var request = require('request');
var multer = require('multer');
var Jimp = require('jimp');
var router = express.Router();
var distance = require('google-distance');
var geolib = require('geolib')

var _= require('underscore');
distance.apiKey = 'AIzaSyA4FufWjJ7wpq6fllBqLULYf9MNwaB1eGE';
var userModel = require('../../../models/userModel');
var dictionariesModel = require('../../../models/dictionariesModel');


var data = {};
var item = {}
//var item = {};
var userId;
var fileData;
var imgData = {}
var currentName;
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        fileData = file;

        var uploadPath = './uploads/' + userId;
        fs.exists(uploadPath, function (exists) {
            if (exists) {
                cb(null, uploadPath)
            }
            else {
                fs.mkdir(uploadPath, function (err) {
                    if (err) {
                        console.log('Error in folder creation');
                        next();
                    }
                    cb(null, uploadPath)
                })
            }
        })

    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();

        currentName = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1];

        item = {
            originalName: file.originalname,
            currentName: currentName
        }

        cb(null, currentName)
    }
});
var upload = multer({ //multer settings
    storage: storage
}).single('file');
/** API path that will uploads the files */
// router.post('/api/userProfile/:id/files', function(req, res) {
//
// });

router.post('/api/userProfile/:id/files', isLoggedIn, function (req, res, next) {

    if (req.user._id == userId) {
        upload(req, res, function (err) {
            if (err) {
                res.json({error_code: 1, err_desc: err});
                return;
            } else {

                userModel.findByIdAndUpdate(userId, {
                    $push: {gallery: item}
                }, {safe: true, upsert: true, new: true}, function (err, model) {
                    console.log(err);
                    res.json({"hej": "sdf"})
                });
            }
            Jimp.read("./uploads/" + userId + "/" + currentName, function (err, image) {
                if (err) throw err;
                image.resize(Jimp.AUTO, 250)
                    .quality(85)// resize
                    .write("./uploads/" + userId + "/thumb-" + currentName); // save
            });
        })
    }
    var data = new userModel(imgData);

})
router.post('/api/userProfile/:id/friends', isLoggedIn, function (req, res, next) {
    var followingData = {
        user: req.params.id
    }

    userModel.findOne({_id: req.user._id}, {following: {$elemMatch: {'user': req.params.id}}}, function (err, result) {
        if (err)
            res.send(err);
        if (result.following.length > 0) {
            console.log("juz obserwujesz tego uzytkownika", result)
        } else {
            console.log("nie obserwujesz", result)
            userModel.findOne({_id: req.user._id}, function (err, user) {
                user.following.push(followingData)
                user.save(function (err, suc) {
                    if (err)
                        console.log(err);

                });
            })

        }
    });


    var followerData = {
        user: req.user._id
    }
    userModel.findOne({_id: req.params.id}, {followers: {$elemMatch: {'user': req.user._id}}}, function (err, result) {
        if (err)
            res.send(err);
        if (result.followers.length > 0) {
            console.log("juz obserwujesz tego uzytkownika", result)
        } else {
            console.log("nie obserwujesz", result)
            userModel.findOne({_id: req.params.id}, function (err, user) {
                user.followers.push(followerData)
                user.save(function (err, suc) {
                    if (err)
                        console.log(err);
                    res.json({
                        isFriend: true
                    })
                });
            })

        }
    });
})

router.delete('/api/userProfile/:id/friends', isLoggedIn, function (req, res, next) {

    userModel.findOneAndUpdate({_id: req.user._id},
        {$pull: {following: {user: req.params.id}}}, {new: true}, function (err, productcontact) {
            if (err)
                res.send(err);
            console.log(":D")
        })
    userModel.findOneAndUpdate({_id: req.params.id},
        {$pull: {followers: {user: req.user._id}}}, {new: true}, function (err, productcontact) {
            if (err)
                res.send(err);
            console.log("asd")
        })
})


router.post('/api/userProfile/:id', isLoggedIn, function (req, res, next) {


    var personalData = {
        date: req.body.userData.date,
        placeOfBirth: {
            name: req.body.placeOfBirth.name,
            latitude: req.body.placeOfBirth.hasOwnProperty("components") ? req.body.placeOfBirth.components.location.lat : req.body.placeOfBirth.latitude,
            longitude: req.body.placeOfBirth.hasOwnProperty("components") ? req.body.placeOfBirth.components.location.long : req.body.placeOfBirth.longitude

        },
        placeOfResidence: {
            name: req.body.placeOfResidence.name,
            latitude: req.body.placeOfResidence.hasOwnProperty("components") ? req.body.placeOfResidence.components.location.lat : req.body.placeOfResidence.latitude,
            longitude: req.body.placeOfResidence.hasOwnProperty("components") ? req.body.placeOfResidence.components.location.long : req.body.placeOfResidence.longitude
        },
        height: req.body.userData.height,
        hair: req.body.userData.hair,
        eyes: req.body.userData.eyes,
        figure: req.body.userData.figure,
        educationLevel: req.body.userData.educationLevel,
        schoolType: req.body.userData.schoolType,
        dateOfCompletion: req.body.userData.dateOfCompletion,
        hobbies: req.body.userData.hobbiesList !== undefined ? req.body.userData.hobbiesList : req.body.userData.hobbies,
        job: req.body.userData.jobsList !== undefined ? req.body.userData.jobsList : req.body.userData.job
    }

    // var data = new userModel(item);
    userModel.findOneAndUpdate({_id: req.params.id}, personalData, {upsert: true}, function (err, result) {

        if (err)
            console.log(err);
        res.json(result)

    })
})
router.get('/api/hobbies', isLoggedIn, function (req, res, next) {
    dictionariesModel.find({}, function (err, result) {
        res.json(result[0].hobbies)
    })
})
router.get('/api/basicDictionary', isLoggedIn, function (req, res, next) {
    dictionariesModel.find({}, function (err, result) {
        res.json({
            hair: [result[0]._doc.hair],
            eyesColors: [result[0]._doc.eyesColors],
            educationLevel: [result[0]._doc.educationLevel],
            figure: [result[0]._doc.figure]
        })
    })
})
router.get('/api/jobs', isLoggedIn, function (req, res, next) {
    dictionariesModel.find({}, function (err, result) {
        res.json(result[0].jobs)
    })
})
router.get('/api/universityCourses', isLoggedIn, function (req, res, next) {
    dictionariesModel.find({}, function (err, result) {
        res.json(result[0].universityCourses)
    })
})
router.get('/api/userProfile/:id', isLoggedIn, function (req, res, next) {


    var currentPoints = 0

    userId = req.params.id;
    var isFriend = false;
    var test = [];
    var currentUserCoords = [];
    userModel.findOne({_id: req.user._id}, {following: {$elemMatch: {'user': req.params.id}}}, function (err, result) {
        if (err)
            res.send(err);
        if (result.following.length > 0) {
            isFriend = true;
        }
    });

    userModel.findOne({_id: req.user._id}, function (err, result) {
        if (err)
            res.send(err);
        currentUserCoords.longitude = result.placeOfResidence.longitude;
        currentUserCoords.latitude = result.placeOfResidence.latitude;
    });
    userModel.findOne({_id: req.params.id}, function (err, result) {
        if (err)
            res.send(err);
        if (req.params.id != req.user._id) {
            if (result.lastVisitors.length < 5) {
                result.lastVisitors.unshift({user: req.user._id})
            } else {
                result.lastVisitors.splice(result.lastVisitors.length-1,1)
                result.lastVisitors.unshift({user: req.user._id})

            }
        }
        result.save()
        var alienDate = new Date(result.date);
        var myDate = new Date(req.user.date);
        var ageDifference;
        var alienYear = alienDate.getFullYear();
        var myYear = myDate.getFullYear();
        if (alienYear >= myYear) {
            ageDifference = alienYear - myYear;
        } else {
            ageDifference = myYear - alienYear;

        }
        if (ageDifference < 2) {
            currentPoints += 3;
        } else if (ageDifference < 3) {
            currentPoints += 2;
        } else if (ageDifference < 5) {
            currentPoints += 1;
        } else if (ageDifference < 7) {
            currentPoints += 0.5;
        }

        for (var i = 0; i < result._doc.hobbies.length; i++) {
            for (var j = 0; j < req.user._doc.hobbies.length; j++) {
                if (req.user._doc.hobbies[j] == result._doc.hobbies[i]) {
                    currentPoints += 0.5;
                }

            }

        }

        if (req.user.gender != result.gender && req.user._id != req.params.id) {
            if (result.gallery.length > 2) {
                currentPoints += 2;
            } else if (result.gallery.length > 0) {
                currentPoints += 1
            }
            if (result.schoolType == req.user.schoolType) {
                currentPoints += 1;
            }
            // distance.get(
            //     {
            //         origin: ''+result.placeOfResidence.latitude+','+result.placeOfResidence.longitude,
            //         destination: ''+currentUserCoords.latitude+','+currentUserCoords.longitude
            //     },
            //     function(err, data) {
            //         if (err) return console.log(err);
            //         console.log(data);
            //     });

            var item = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' + result.placeOfResidence.latitude + ',' + result.placeOfResidence.longitude + '&destinations=' + currentUserCoords.latitude + ',' + currentUserCoords.longitude + '&key=AIzaSyA4FufWjJ7wpq6fllBqLULYf9MNwaB1eGE';
            if(result.placeOfResidence.latitude.length>0&&currentUserCoords.latitude.length>0){
            var userDistance =  geolib.getDistanceSimple(
                {latitude: result.placeOfResidence.latitude, longitude: result.placeOfResidence.longitude },
                {latitude: currentUserCoords.latitude, longitude: currentUserCoords.longitude}
            );
            if (Math.round(userDistance / 1000) <= 50) {
                // currentPoints += 1;
                currentPoints +=2;
            } else if (Math.round(userDistance / 1000) <= 100) {
                // currentPoints += 0.5;
                currentPoints +=1;
                // return distanceBetween = Math.round(element.distance.value) / 1000;
            }
            }
            res.json({
                result: result,
                isFriend: isFriend,
                userId: req.user._id,
                currentPoints: currentPoints
            })
            // request(item, function (error, response, body) {
            //     if (!error && response.statusCode == 200) {
            //         var element = JSON.parse(body).rows[0].elements[0];
            //         if (element.hasOwnProperty('distance')) {
            //             if (Math.round(element.distance.value / 1000) <= 50) {
            //                 currentPoints += 2;
            //             } else if (Math.round(element.distance.value / 1000) <= 100) {
            //                 currentPoints += 1;
            //             }
            //         }
            //         console.log("current pointes", currentPoints)
            //         test.push({points: currentPoints})
            //         // Print the google web page.
            //         // res.json({)
            //         res.json({
            //             result: result,
            //             isFriend: isFriend,
            //             userId: req.user._id,
            //             currentPoints: currentPoints
            //         })
            //     }
            // })

        } else {
            res.json({
                result: result,
                isFriend: isFriend,
                userId: req.user._id,
                currentPoints: 0
            })
        }
        console.log(test)
        // var test =  geolib.getPathLength([
        //     {latitude: result.placeOfResidence.latitude, longitude: result.placeOfResidence.longitude}, // Berlin
        //     {latitude: currentUserCoords.latitude, longitude: currentUserCoords.longitude}, // Dortmund
        // ]);
        // console.log("odlegos", test)
    });


});
router.get('/api/userProfile/:id/gallery/avatar/', isLoggedIn, function (req, res, next) {

    userModel.findOne({_id: req.params.id}).exec(function (err, result) {
        if (err)
            res.send(err);
        if (result.avatar) {
            res.send(result.avatar)
        }

    });


});
router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;