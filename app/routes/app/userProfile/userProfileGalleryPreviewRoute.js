/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');
var fs = require('fs');
var express = require('express');
var _ = require('underscore');
var multer = require('multer');
var Jimp = require('jimp');
var mongoose = require('mongoose');
var router = express.Router();


var userModel = require('../../../models/userModel');

var data = {};
var item = {}
//var item = {};
var userId;
var fileData;
var imgData = {}
var currentName;

router.get('/api/userGallery/:id/picture/:image', isLoggedIn, function (req, res, next) {
    userId = req.params.id;
    var imgId = req.params.image;
    var previousImage, nextImage,imageAuthor;

    var minimumObjectId = new mongoose.Types.ObjectId(req.params.image);

    userModel.findOne({_id: req.params.id}, {gallery: {$elemMatch: {'_id': {$gt: req.params.image}}}}).sort({_id: 1}).exec(function (err, data) {
        if (data.gallery[0]) {
            nextImage = data.gallery[0]._id;
        }
    })
    userModel.findOne({_id: req.params.id}).sort({_id: -1}).exec(function (err, data) {
        if (data.gallery[0]) {
          for(var i=0; i<=data.gallery.length-1;i++){
              if(data.gallery[i]._id == req.params.image){
                  if(previousImage = data.gallery[i-1]){
                  previousImage = data.gallery[i-1]._id;
                  }
              }

          }

        }
    })
    userModel.findOne({_id: req.params.id}).exec(function (err, data) {
            imageAuthor = data;

    })
    userModel.findOne({_id: req.params.id}, {gallery: {$elemMatch: {'_id': req.params.image}}}).populate("gallery.comments.author").exec(function (err, result) {
        if (err)
            res.send(err);

        res.json({
            imageAuthor: imageAuthor,
            imageData: result.gallery[0],
            previous: previousImage,
            next: nextImage,
            currentUser: req.user
        })

    });

});

router.post("/api/userGallery/:id/picture/:image/comment/add", isLoggedIn, function (req, res, next) {

    var commentData = {
        author: req.user.id,
        content: req.body.commentData
    }

    userModel.findOne({_id: req.params.id}, function (err, result) {
        result.gallery.id(req.params.image).comments.unshift(commentData)
        result.save(function (err, result) {
            if (err)
                console.log(err)
            userModel.findOne({_id: req.params.id}).populate("gallery.comments.author").exec(function (err, result) {
                res.json(result.gallery.id(req.params.image))
            })


        });
    })
})


router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;