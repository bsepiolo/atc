/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');
var fs = require('fs');
var express = require('express');
var multer = require('multer');
var Jimp = require('jimp');
var router = express.Router();


var userModel = require('../../../models/userModel');

router.delete('/api/userProfile/:id/friends', isLoggedIn, function (req, res, next) {

    userModel.findOneAndUpdate({_id: req.user._id},
        {$pull: {following: {user: req.params.id}}}, {new: true}, function (err, productcontact) {
            if (err)
                res.send(err);
            res.json({message: 'Role successfully deleted.'});
        })
    // userModel.findOne({_id: req.user._id}, {'following.user': req.params.id}, function (err, result) {
    //     result.following.user(req.user._id).remove();
    //     result.save();
    // })


    // userModel.findOne({_id: req.user._id}, {following: {$elemMatch: {'user': req.params.id}}}).remove().exec();
})
router.get('/api/userProfile/:id/following', isLoggedIn, function (req, res, next) {
    userModel.findOne({_id: req.params.id}).populate("following.user").exec(function (err, result) {
        if (err)
            res.send(err);
        res.json(result.following)

    });
});
router.get('/api/userProfile/:id/followers', isLoggedIn, function (req, res, next) {
    userModel.findOne({_id: req.params.id}).populate("followers.user").exec(function (err, result) {
        if (err)
            res.send(err);
        res.json(result.followers)

    });
});
// router.post('/api/userProfile/:id/gallery/avatar/:image', isLoggedIn, function (req, res, next) {
//
//         userModel.find({_id: req.params.id}).populate("gallery.comments.author").exec(function (err, result) {
//             if (err)
//                 res.send(err);
//
//             var personalData = {
//                 avatar: result.gallery[0]
//             }
//             userModel.findOneAndUpdate({_id: req.params.id}, personalData, {upsert: true}, function (err, result) {
//
//                 if (err)
//                     res.send(err);
//                 res.json(result)
//
//             })
//
//         });
//         // var data = new userModel(item);
//
//         console.log(req.body)
//
// })


router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;