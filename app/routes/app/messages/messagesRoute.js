/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
module.exports = function (app, io) { // catch here
    var passport = require('passport');
    var express = require('express');
    //  var app = express();
    var fs = require('fs');

    var multer = require('multer');
    var Jimp = require('jimp');
    var router = express.Router();
    // console.log(userId)

    var messageModel = require('../../../models/messageModel');

    var conversationId = {};

    var userData;
    var currentUser;
    var chatData = [];
    //router.get("api/messages/:id", function (req, res) {

    // router.get('/api/messages/:id', function (req, res, next) {
    //
    //     console.log("WESZLO")
    //     messageModel.findOne({
    //         $or: [{user1: req.user._id, user2: req.params.id}, {
    //             user1: req.params.id,
    //             user2: req.user._id
    //         }]
    //     }).populate('messages.from').exec(function (err, messages) {
    //         if (messages == null) {
    //             messageModel.create({
    //                 user1: req.user._id,
    //                 user2: req.params.id
    //             }, function (err, small) {
    //                 if (err) return handleError(err);
    //                 // saved!
    //             })
    //         } else {
    //
    //
    //
    //             console.log("juz istnieje", messages)
    //         }
    //         messageModel.find({
    //         $or: [{user1: req.user._id}, {user2: req.user._id}]
    //     }).populate('messages.from').populate('user1').populate('user2').select({"messages": {"$slice": -1}}).limit(5).exec(function (err, converation) {
    //
    //             res.json({messages:messages,conversations: converation})
    //     })
    //     })
    //     //res.json(req.user._id)
    // });
    // router.get('/api/messages/:id', function (req, res, next) {
    // messageModel.findOne({
    //     $or: [{user1: req.user._id, user2: req.params.id}, {
    //         user1: req.params.id,
    //         user2: req.user._id
    //     }]
    // }).populate('messages.from').exec(function (err, result) {
    //     if (result == null) {
    //         messageModel.create({
    //             user1: req.user._id,
    //             user2: req.params.id
    //         }, function (err, small) {
    //             if (err) return handleError(err);
    //             // saved!
    //         })
    //     } else {
    //
    //
    //
    //         console.log("juz istnieje", result)
    //     }
    // })
    //
//     // })
    var stopLoading;
    var chat = io.of('/chat');
    app.use("/messages/:id", function (req, res, next) {
        if (req.method === 'GET') {
            currentUser = req.user._id;
            chatData = [];
            console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
            console.log("piszę do", req.user._id);
            console.log("piszę do", req.params.id);


            next()
        }
    })
    var connectedClients = {};
    var people = {};
    chat.on('connection', function (socket) {

        socket.on('leaveRoom', function (data) {
            stopLoading = data.stopLoading;
            loadMessages(data.toSendId, data.to, data.currentUserId, socket, leave = true)

        })

        socket.on('welcome', function (data) {
            chat.emit('connection authorized', chatData);
        })


        // socket.on("load history", function (data) {
        //     console.log("data", data)
        //     //if(stopLoading!=true) {
        //     loadConversation(data.currentUserId,data.to, socket, loadHeader = false)
        //     //  }
        // });
        socket.on('hello from header', function (data) {
            console.log("heade TEEEST", data.currentUserId)
            messageModel.find({
                $or: [{user1: data.currentUserId}, {user2: data.currentUserId}]
            }).populate('messages.from').populate('user1').populate('user2').select({"messages": {"$slice": -1}}).limit(5).exec(function (err, result) {
                socket.emit('header messages', result);
            })
            socket.emit("header conversation");
        })
        // socket.emit("header conversation");

        socket.on('get conversation list', function (data) {
            loadConversation(data.currentUserId, data.to, socket)
        })

        socket.on('load header conversation', function (data) {


            people[data.currentUserId] = socket.id;

            //loadConversation(data.currentUserId,data.to, socket)
        })
        socket.on('message', function (data) {
            socket.emit("header conversation");
            var conId;
            messageModel.findOne({$or: [{user1: data.from, user2: data.to}, {
                user1: data.to,
                user2: data.from
            }]}, function (err, result) {
                conId = result._id;
                result.messages.push({message: data.message, from: data.from})
                result.readed = "No";
                result.save(function (err, suc) {
                    //select({ "messages": { "$slice": -1 }})
                    messageModel.findOne({$or: [{user1: data.from, user2: data.to}, {
                        user1: data.to,
                        user2: data.from
                    }]}).populate("messages.from").select({"messages": {"$slice": -1}}).exec(function (err, post) {
                        console.log("to ja", post.messages[0]);
                        if (err)
                            console.log(err)

                        chat.to(conId).emit("messageData", post.messages[0])

                        messageModel.find({
                            $or: [{user1: data.to}, {user2: data.to}]
                        }).populate('messages.from').populate('user1').populate('user2').select({"messages": {"$slice": -1}}).exec(function (err, result) {

                            chat.to(people[data.to]).emit('header messages', result);


                            console.log("juz istnieje", result)

                        })
                    })

                })

            });


        })

        socket.on("header leaveroom", function (data) {
            if (socket.lastRoom) {
                socket.leave(socket.lastRoom)
                socket.lastRoom = null;
            }
        })
        socket.on("load messages history", function (data) {
            if (stopLoading != true) {
                loadMessages(data.toSendId, data.to, data.currentUserId, socket, leave = false)
            }
        })


    });
    function loadMessages(conversationId, receiver, sender, socket, leaveRoom) {
      var conId;
        messageModel.findOne({
            $or: [{user1: sender, user2: receiver}, {
                user1: receiver,
                user2: sender
            }]
        }).populate('messages.from').exec(function (err, result) {
            if (result == null && sender !== undefined && receiver !== undefined) {
                messageModel.create({
                    user1: sender,
                    user2: receiver
                }, function (err, small) {
                    if (err) return handleError(err);
                    // saved!
                    // loadConversation(sender, socket, loadHeader = false)
                    messageModel.findOne({
                        $or: [{user1: sender, user2: receiver}, {
                            user1: receiver,
                            user2: sender
                        }]
                    }, function (err, result) {
                      //.populate('messages.from')
                        conId = result._id;
                        socket.emit("chatRoom", result)

                    })

                })
            } else if (sender !== undefined && receiver !== undefined) {
                conId = result._id;
                if (socket.lastRoom && leaveRoom == true) {
                    socket.leave(socket.lastRoom)
                    socket.lastRoom = null;
                }
                if (conId) {
                    socket.join(conId);
                    socket.lastRoom = conId;
                }
                if (result.messages.length > 0) {
                    if (result.messages[result.messages.length - 1].from._id != sender) {
                        result.readed = "True";
                        result.save();
                    }
                }
                socket.emit('chatRoom messages', result);


                console.log("juz istnieje", result)
            }
        })

    }

    function loadConversation(sender, receiver, socket, loadHeader) {
        messageModel.findOne({
            $or: [{user1: sender, user2: receiver}, {
                user1: receiver,
                user2: sender
            }]
        }).populate('messages.from').exec(function (err, result) {

            if (result == null && sender !== undefined && receiver !== undefined) {
                messageModel.create({
                    user1: sender,
                    user2: receiver
                }, function (err, small) {
                    if (err) return handleError(err);
                    // saved!
                    // loadConversation(sender, socket, loadHeader = false)
                    // small.messages.push({from: sender, message: "test"})
                    // small.save(function(err){
                    //     console.log(err)
                    // });
                })

            } else if (sender !== undefined && receiver !== undefined) {

                result.save();

                socket.emit('chatRoom messages', result);


                console.log("juz istnieje", result)
            }

            messageModel.find({
                $or: [{user1: sender}, {user2: sender}]
            }).populate('messages.from').populate('user1').populate('user2').select({"messages": {"$slice": -1}}).exec(function (err, result) {
                if (result.length > 0) {
                    if (loadHeader == false) {
                        socket.emit('chatRoom', result);

                    } else {
                        socket.emit('chatRoom', result);

                    }

                } else {
                    socket.emit('chatRoom', {noData: "no data"});

                }

                console.log("juz istnieje", result)

            })
        })


    }


    function isLoggedIn(req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated()) {
            userData = req.user;
            return next();
        }
        // if they aren't redirect them to the home page
        res.status(401).json({
            message: 'not authorized'
        });
        // res.redirect('/login');

    }

    return router;
}
//module.exports = router;