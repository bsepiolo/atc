/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');

var express = require('express');
var _ = require('underscore');
var router = express.Router();
var userModel = require('../../models/userModel');
var messageModel = require('../../models/messageModel');

require('../../../config/passport')(passport); // pass passport for configuration
var data = {};
router.get('/api/roulette', isLoggedIn, function (req, res, next) {
    var genderToFind;
    if (req.user.gender == 'male') {
        genderToFind = 'female'
    } else {
        genderToFind = 'male'
    }
    userModel.find({gender: genderToFind, avatar: {$exists: true, $not: {$size: 0}}}, function (err, result) {
        var randomUser = _.sample(result);
        if (randomUser !== undefined) {
            messageModel.findOne({
                $or: [{user1: req.user._id, user2: randomUser._id}, {
                    user2: req.user._id,
                    user1: randomUser._id
                }]
            }, function (err, result) {

                if (result == null) {
                    messageModel.create({
                        user1: req.user._id,
                        user2: randomUser._id
                    }, function (err, small) {
                        if (err) return handleError(err);
                        messageModel.findOne({
                            $or: [{user1: req.user._id, user2: randomUser._id}, {
                                user2: req.user._id,
                                user1: randomUser._id
                            }]
                        },function(err,result){
                            result.messages.push({message: "Hello! " + randomUser.firstName, from: req.user._id})
                            result.readed = "No";
                            result.save(function (err, suc) {
                                //select({ "messages": { "$slice": -1 }})
                            })
                        })
                    })


                }else{
                    result.messages.push({message: "Hello! " + randomUser.firstName, from: req.user._id})
                    result.readed = "No";
                    result.save(function (err, suc) {
                        //select({ "messages": { "$slice": -1 }})


                    })
                }


            });
            res.json(randomUser);
        }
    })

});
router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;