/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');

var express = require('express');
var _ = require('underscore');
var router = express.Router();
var userModel = require('../../models/userModel');

require('../../../config/passport')(passport); // pass passport for configuration
var data = {};
router.get('/api/appHeader', isLoggedIn, function (req, res, next) {
    userModel.findOne({_id: req.user._id}).populate("followers.user").exec(function (err, result) {
        if (err)
            res.send(err);
        res.json(result)

    });
   // res.json(data);
    console.log("lololololo")
});
router.post('/api/appHeader/notification/readed', isLoggedIn, function (req, res, next) {
    userModel.findOne({_id: req.user._id},{'followers.user': req.body.notificationId}).populate("followers.user").exec(function (err, result) {
        if (err)
            res.send(err);
        // result.followers[0].readed = 'True';
        // result.save(function(err){
        //     console.log(err)
        // })
        var even = _.find(result.followers, function(num){ return num.user._id == req.body.notificationId; });
        var indexOf = _.indexOf(result.followers, even);
        result.followers[indexOf].readed="True";

        result.save(function(err,result){
            console.log(err)
            userModel.findOne({_id: req.user._id}).populate("followers.user").exec(function (err, result) {
                if (err)
                    res.send(err);
                res.json(result)

            });
        });


    });
    console.log("")
})
router.post('/api/appHeader/search', isLoggedIn, function (req, res, next) {
    var re = new RegExp("/\s/g");
    var searchString={};
    if(/\s/g.test(req.body.searchData)){
        searchString = req.body.searchData.split(" ")
        console.log("DDDDDDDD", searchString)
    }
    if(searchString.length>0){
        userModel.find({$or:[{firstName: {$regex: new RegExp('.*'+searchString[0]+'.*', 'i')},lastName: {$regex: new RegExp('.*'+searchString[1]+'.*', 'i')}},{firstName: {$regex: new RegExp('.*'+searchString[1]+'.*', 'i')},lastName: {$regex: new RegExp('.*'+searchString[0]+'.*', 'i')}} ]}, function (err, result) {
            if(result.length>0) {

                res.json(result)
            }
        })
    }else{
        if(searchString.toString().length>3){
        userModel.find({$or:[{firstName: {$regex: new RegExp('.*'+req.body.searchData+'.*', 'i')}},{lastName: {$regex: new RegExp('.*'+req.body.searchData+'.*', 'i')}} ]}, function (err, result) {
            if(result.length>0){
            res.json(result)
            }
        })
        }
    }
})
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;