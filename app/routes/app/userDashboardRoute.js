/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var passport = require('passport');

var express = require('express');
var _ = require('underscore');
var router = express.Router();
var userModel = require('../../models/userModel');
var postModel = require('../../models/postModel');

require('../../../config/passport')(passport); // pass passport for configuration
var data = {};
var lastSeen = null;
router.get('/api/userDashboard', isLoggedIn, function (req, res, next) {

   // res.json(data);
    console.log("dashboard")
    userModel.findOne({_id: req.user._id}).populate("lastVisitors.user").exec(function (err, result) {

            res.json(result)

    });
});
router.get('/api/friendsList', isLoggedIn, function (req, res, next) {
    userModel.findOne({_id: req.user._id}).populate("following.user").exec(function (err, result) {
        if (err)
            res.send(err);
        res.json(result.following)

    });
})
router.get('/api/userDashboard/posts', isLoggedIn, function (req, res, next) {
    var followingList;

    userModel.findOne({_id: req.user._id}, function (err, result) {

        followingList = result.following;
        // var arr = followersList.map(obj, function(el) { return el });
        var newArr = followingList.map(function (element) {
            return element.user;
        });
        postModel.find({$or: [{author: {"$in": newArr}}, {author: req.user._id}]}, {comments: {$slice: 3}}).sort({"date": -1}).populate("author").limit(5).populate("comments.author").exec(function (err, result) {
            lastSeen = 5;

            res.json(result)
        })
    });
})
router.delete('/api/userDashboard/posts/:id', isLoggedIn, function (req, res, next) {
    var followingList;

    postModel.findOne({_id: req.params.id}).remove().exec(function(err,result){
        userModel.findOne({_id: req.user._id}, function (err, result) {

            followingList = result.following;        // var arr = followersList.map(obj, function(el) { return el });
            var newArr = followingList.map(function (element) {
                return element.user;
            });
            postModel.find({$or: [{author: {"$in": newArr}}, {author: req.user._id}]}, {comments: {$slice: 3}}).sort({"date": -1}).populate("author").limit(5).populate("comments.author").exec(function (err, result) {
                lastSeen = 5;

                res.json(result)
            })
        })
    })
});
router.get('/api/userDashboard/post/:postId/comments/:loadNumber', isLoggedIn, function (req, res, next) {

    postModel.findOne({_id: req.params.postId}, {comments: {$slice: [parseInt(req.params.loadNumber), 3]}}).sort({"date": -1}).populate("author").limit(5).populate("comments.author").exec(function (err, result) {
        res.json(result.comments)
    })
});
router.get('/api/userDashboard/morePosts', isLoggedIn, function (req, res, next) {
    var followingList;
    userModel.findOne({_id: req.user._id}, function (err, result) {
        followingList = result.following;        // var arr = followersList.map(obj, function(el) { return el });
        var newArr = followingList.map(function (element) {
            return element.user;
        });
        postModel.find({$or: [{author: {"$in": newArr}}, {author: req.user._id}]}, {comments: {$slice: 3}}).sort({"date": -1}).populate("author").skip(lastSeen).limit(5).populate("comments.author").exec(function (err, result) {
            lastSeen += 5;

            res.json(result)
            console.log("aaaaaaaaaaaaaaaaa", result)
        })
    })
});
router.post('/api/userDashboard/like/add', isLoggedIn, function (req, res, next) {
    postModel.findOne({_id: req.body.postId, 'likes.author': req.user._id}, function (err, result) {

        //console.log("ssssssssss", even)
        if (result == null) {
            postModel.findOne({_id: req.body.postId}, function (err, result) {
                result.likes.unshift({author: req.user._id})
                result.save(function (err, result) {
                    if (err)
                        console.log(err)
                    postModel.findOne({_id: req.body.postId}).populate("comments.author").exec(function (err, result) {
                        res.json({likes:result.likes,liked: true})
                    })
                });
            })
        }
    })
})
router.post('/api/userDashboard/posts/comments/add', isLoggedIn, function (req, res, next) {
    var commentData = {
        author: req.user.id,
        content: req.body[1].commentData
    }

    postModel.findOne({_id: req.body[0].postId}, function (err, result) {
        result.comments.unshift(commentData)
        result.save(function (err, result) {
            if (err)
                console.log(err)
            postModel.findOne({_id: req.body[0].postId}).populate("comments.author").exec(function (err, result) {
                res.json(result.comments[0])
            })
        });
    })
})
router.post('/api/userDashboard/posts/add', isLoggedIn, function (req, res, next) {
    //res.json(data);
    var postData = {
        author: req.user._id,
        type: req.body[1].postType,
        attachments: [{
            pictureOriginalName: req.body[0].postData.pictureOriginalName,
            pictureCurrentName: req.body[0].postData.pictureCurrentName,
            textContent: req.body[0].postData.textContent,
            latitude: req.body[0].postData.latitude,
            longitude: req.body[0].postData.longitude,
            locationName: req.body[0].postData.locationName
        }]
    }
    postModel.create(postData, function (err, small) {
        if (err) return handleError(err);
        // saved!
        var followingList;

        userModel.findOne({_id: req.user._id}, function (err, result) {

            followingList = result.following;
            // var arr = followersList.map(obj, function(el) { return el });
            var newArr = followingList.map(function (element) {
                return element.user;
            });
            postModel.find({$or: [{author: {"$in": newArr}}, {author: req.user._id}]}, {comments: {$slice: 3}}).sort({"date": -1}).populate("author").limit(5).populate("comments.author").exec(function (err, result) {
                lastSeen = 5;

                res.json(result)
            })
        });
    })
    console.log("dashboard")
});
router.get('/api/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
function isLoggedIn(req, res, next) {
    // console.log(req)

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        data = req.user;
        return next();
    }
    // if they aren't redirect them to the home page
    res.status(401).json({
        message: 'not authorized'
    });
    // res.redirect('/login');

}
module.exports = router;