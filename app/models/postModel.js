/**
 * Created by Bartosz Sepioło on 12.07.2017.
 */
var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
    date: {type: Date, default: Date.now},
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    type: {type: String, default: ''},
    attachments: [{
        pictureOriginalName: {type: String, default: ''},
        pictureCurrentName: {type: String, default: ''},
        textContent: {type: String, default: ''},
        latitude: {type: String, default: ''},
        longitude: {type: String, default: ''},
        locationName: {type: String, default: ''}
    }],
    likes: [{
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        },
        date: {type: Date, default: Date.now}
    }]
    ,
    comments: [{
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        },
        date: {type: Date, default: Date.now},
        content: {type: String, default: ''}
    }]
});
module.exports =  mongoose.model('PostModel',PostSchema);