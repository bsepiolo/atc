/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var hobbiesSchema = new Schema({
    "name": {type: String, default: ''}
});
var jobsSchema = new Schema({
    "name": {type: String, default: ''}
});
var universityCoursesSchema = new Schema({
    "name": {type: String, default: ''}
});

var dictionarySchema = new Schema({
    hobbies: [hobbiesSchema],
    jobs: [jobsSchema],
    universityCourses: [universityCoursesSchema]
});

// userSchema.methods.generateHash = function(password) {
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };
//
// // checking if password is valid

module.exports = mongoose.model('dictionaries', dictionarySchema);