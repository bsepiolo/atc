/**
 * Created by Bartosz Sepioło on 12.07.2017.
 */
var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
    user1: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    user2: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    messages: [{
        from:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        },
        message: String,
        date: {type: Date, default: Date.now()},
    }],
    readed: {type: String, default: "No"}
});
module.exports =  mongoose.model('MessageModel',MessageSchema);