/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var gallerySchema = new Schema({
    originalName: {type: String, default: ''},
    currentName: {type: String, default: ''},
    date: {type: Date, default: Date.now},
    isAvatar: {type: Boolean, default: false},
    comments: [{
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        },
        date: {type: Date, default: Date.now},
        content: {type: String, default: ''}
    }]
});

var userSchema = new Schema({
    email: {type: String, default: ''},
    firstName: {type: String, default: ''},
    lastName: {type: String, default: ''},
    gender: {type: String, default: ''},
    date: {type: String, default: ''},
    password: {type: String, default: ''},
    role: {type: String, default: 'User'},
    placeOfBirth: {
        name: {type: String, default: ''},
        latitude: {type: String, default: ''},
        longitude: {type: String, default: ''}
    },
    placeOfResidence: {
        name: {type: String, default: ''},
        latitude: {type: String, default: ''},
        longitude: {type: String, default: ''}
    },
    height: {type: String, default: ''},
    hair: {type: String, default: ''},
    eyes: {type: String, default: ''},
    figure: {type: String, default: ''},
    educationLevel: {type: String, default: ''},
    schoolType: {type: String, default: ''},
    dateOfCompletion: {type: Date, default: ''},
    hobbies: [{type: String, default: ''}],
    job: {name: {type: String, default: ''}},
    gallery: [gallerySchema],
    lastVisitors: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        },
        date: {type: Date, default: Date.now}
    }],
    following: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        }
    }],
    followers: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        },
        readed: {type: String, default: 'No'}
    }],
    avatar: gallerySchema
});

// userSchema.methods.generateHash = function(password) {
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };
//
// // checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};
module.exports = mongoose.model('users', userSchema);