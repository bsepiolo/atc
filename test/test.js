'use strict';
var passport = require('passport');

const chai = require('chai');
const expect = require('chai').expect;
var io = require('socket.io')
const request = require('supertest');
var server = request.agent('http://localhost:3000');
var bcrypt = require('bcrypt');

chai.use(require('chai-http'));
require('../config/passport')(passport); // pass passport for configuration
var LocalStrategy = require('passport-local').Strategy;

const app = require('../server.js'); // Our app
describe('GET /api/getDir', function(){

    // it('should return login error', function(done){
    //     server
    //         .post('/api/login')
    //         .send({email: 'nieistniejacy@pl.pl', password: 'Test.1234'})
    //         .expect(200)
    //         .end(onResponse);
    //
    //     function onResponse(err, res) {
    //         if (err) return done(err);
    //         return done();
    //     }
    // });
    it('should return Not Found', function(done) {
        server
            .get('/NIE_Ma_TAKIEGO_ZASOBU')
            .expect({})
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                //     registeredUser = res.body.result._id
                done()
            });
    });
    it('should return register error', function(done){
        var item = {
            email: "nieznajom3y@test.pl",
            firstName: "John",
            lastName: "Kowalsky",
            gender: "male",
            date: "12"
          //  password: bcrypt.hashSync("test", 8)
        }
        server
            .post('/api/register')
            .send(item)
            .expect(500)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
           //     registeredUser = res.body.result._id
                done()
            });
    });
    var registeredUser;
    it('should register user', function(done){
        var item = {
            email: "nieznajomy@test.pl",
            firstName: "John",
            lastName: "Kowalsky",
            gender: "male",
            date: "12",
            password: bcrypt.hashSync("test", 8)
        }
        server
            .post('/api/register')
            .send(item)
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                registeredUser = res.body.result._id
                done()
            });
    });
    it('login', loginUser());


    it('should remove registered user', function(done){
        var item = {
            email: "nieznajomy@test.pl",
            firstName: "John",
            lastName: "Kowalsky",
            gender: "male",
            date: "12",
            password: bcrypt.hashSync("test", 8)
        }
        server
            .delete('/api/admin/users/'+registeredUser)
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });

    it('should return basic user data', function(done){
        server
            .get('/api/userDashboard')
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
    it('should return top 5 posts', function(done){
        server
            .get('/api/userDashboard/posts')
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
    it('should return 5 more posts', function(done){
        server
            .get('/api/userDashboard/morePosts')
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
    var addedPostId;
    it('should add new post', function(done){
        var postData = [{
            postData: {

                textContent: "Testowy post"

            }
        },{
            postType: "text"
        }]
        server
            .post('/api/userDashboard/posts/add')
            .send(postData)
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                addedPostId = res.body[0]._id;
                    done()
            });
    });
    it('should add like to post', function(done){
        var postData = {
            postId: addedPostId
        }
        server
            .post('/api/userDashboard/like/add')
            .send(postData)
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                //addedPostId = res.body[0]._id;
                done()
            });
    });
    it('should add comment to post', function(done){
        var postData = [{
            postId: addedPostId
        },{
            commentData: "Testowy komentarz"
        }]
        server
            .post('/api/userDashboard/posts/comments/add')
            .send(postData)
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                //addedPostId = res.body[0]._id;
                done()
            });
    });
    it('should remove post', function(done) {
        server
            .delete('/api/userDashboard/posts/'+addedPostId)
            .expect(200)
            .end(function(err, res) {
                if (err) return done(err);
                console.log(res.body);
                done()
            })
    })
    it('should return following users', function(done){

        server
            .get('/api/friendsList')
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
    it('should logout', function(done){
        server
            .get('/api/logout')
            .expect(302)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
                //done()
            });
//     });
// })
function loginUser() {
    return function (done) {
        server
            .post('/api/login')
            .send({email: 'terry.sandoval@test.pl', password: 'Test.1234'})
            .expect(200)
          //  .expect('Location', '/')
            .end(onResponse);

        function onResponse(err, res) {
            if (err) return done(err);
            return done();
        }
    }
};


// });
// describe('API endpoint /colors', function() {
//     this.timeout(5000); // How long to wait for a response (ms)
//     var socket;
//
//     beforeEach(function() {
//         return chai.request(app)
//             .post('/api/login')
//             .send({
//                 email: 'bartosz.sepiolo@hotmail.com',
//                 password: 'Test.1234'
//             })
//     })
//         after(function () {
//
//         });
//
//
//     it('should return all colors', function() {
//         return chai.request(app)
//             .post('/api/login')
//             .send({
//                 email: 'bartosz.sepiolo@hotmail.com',
//                 password: 'Test.1234'
//             }).end(function (err,res ){
//                 chai.request(app)
//                     .get('/api/userDashboard')
//                     .then(function(res) {
//                         expect(res).to.have.status(200);
//                         expect(res).to.be.json;
//                         expect(res.body).to.be.an('object');
//                         expect(res.body.results).to.be.an('array');
//                     });
//             })
//         //return
//         // return chai.request(app)
//         //     .get('/api/userDashboard')
//         //     .then(function(res) {
//         //         expect(res).to.have.status(200);
//         //         expect(res).to.be.json;
//         //         expect(res.body).to.be.an('object');
//         //         expect(res.body.results).to.be.an('array');
//         //     });
//     });
//
//     it('should login', function () {
//
//
//     });
//
//     // GET - List all colors
//
//
//     // GET - Invalid path
//     it('should return Not Found', function() {
//         return chai.request(app)
//             .get('/INVALID_PATH')
//             .then(function(res) {
//                 throw new Error('Path exists!');
//             })
//             .catch(function(err) {
//                 expect(err).to.have.status(404);
//             });
//     });
//
//     // // POST - Add new color
//     // it('should add new color', function() {
//     //     return chai.request(app)
//     //         .post('/colors')
//     //         .send({
//     //             color: 'YELLOW'
//     //         })
//     //         .then(function(res) {
//     //             expect(res).to.have.status(201);
//     //             expect(res).to.be.json;
//     //             expect(res.body).to.be.an('object');
//     //             expect(res.body.results).to.be.an('array').that.includes(
//     //                 'YELLOW');
//     //         });
//     // });
//     //
//     // // POST - Bad Request
//     // it('should return Bad Request', function() {
//     //     return chai.request(app)
//     //         .post('/colors')
//     //         .type('form')
//     //         .send({
//     //             color: 'YELLOW'
//     //         })
//     //         .then(function(res) {
//     //             throw new Error('Invalid content type!');
//     //         })
//     //         .catch(function(err) {
//     //             expect(err).to.have.status(400);
//     //         });
//     // });
//});