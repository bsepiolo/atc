var express = require('express');
var passport = require('passport');
var path = require('path');
var http = require('http');
// var favicon = require('serve-favicon');
// var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var morgan = require('morgan');
var flash = require('connect-flash');
var session = require('express-session');
var cors = require('cors');

const MongoStore = require('connect-mongo')(session);

// var sassMiddleware = require('node-sass-middleware');

// var index = require('./routes/index');
// var users = require('./routes/users');

var app = express();
//var server = http.createServer(app);

var io = require('socket.io')(4000)

// view engine setup

var db = require('./config/db');
mongoose.connect(db.url);
require('./config/passport')(passport); // pass passport for configuration
// DZIAŁA - PRZERZUCIĆ DO ROUTERA !!!

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

app.use(methodOverride('X-HTTP-Method-Override'));
var userId;
app.all("/", function(req, res, next) {
    if(req.user){
        userId = req.user._id;
    }

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.use(session({secret: 'foo',
    store: new MongoStore({mongooseConnection: mongoose.connection})})); // session secret
app.use(passport.initialize());
app.use(passport.session(true)); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/bootstrap', express.static(path.join(__dirname, 'public/libs/bootstrap-sass/assets')));


app.use(express.static('public'));

var loginRouter = require('./app/routes/home/loginRoute');
var landingPageRouter = require('./app/routes/home/landingPageRoute');
var registerRouter = require('./app/routes/home/registerRoute');
var userDashboardRouter =  require('./app/routes/app/userDashboardRoute'); // configure our routes
var appHeaderRouter =  require('./app/routes/app/appHeaderRouter'); // configure our routes
var userProfileRouter =  require('./app/routes/app/userProfile/userProfileRoute'); // configure our routes
var userProfileGalleryRouter =  require('./app/routes/app/userProfile/userProfileGalleryRoute'); // configure our routes
var userProfileGalleryPreviewRouter =  require('./app/routes/app/userProfile/userProfileGalleryPreviewRoute'); // configure our routes
var friendsRouter =  require('./app/routes/app/userProfile/friendsRoute'); // configure our routes
var messagesRouter =  require('./app/routes/app/messages/messagesRoute')(app, io); // configure our routes
var browseRouter =  require('./app/routes/app/browseRoute'); // configure our routes
var suggestionsRouter =  require('./app/routes/app/suggestionsRoute'); // configure our routes
var rouletteRouter =  require('./app/routes/app/rouletteRoute'); // configure our routes
var adminUsersRouter =  require('./app/routes/app/admin/usersRoute'); // configure our routes
var adminDictionariesRouter =  require('./app/routes/app/admin/dictionariesRoute'); // configure our routes


app.use('/', loginRouter);
app.use('/', landingPageRouter);
app.use('/', registerRouter);
app.use('/', userDashboardRouter);
app.use('/', appHeaderRouter);
app.use('/', userProfileRouter);
app.use('/', userProfileGalleryRouter);
app.use('/', userProfileGalleryPreviewRouter);
app.use('/', friendsRouter);
app.use('/', messagesRouter);
app.use('/', browseRouter);
app.use('/', suggestionsRouter);
app.use('/', rouletteRouter);
app.use('/', adminUsersRouter);
app.use('/', adminDictionariesRouter);


app.use('*', function (req, res, next) {
    console.log("witej", path.join(__dirname + '/public/views/home/shared/landingPageLayout.html'))
    res.sendFile(path.join(__dirname + '/public/views/home/shared/landingPageLayout.html'));
});

module.exports = app;
