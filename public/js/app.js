/**
 * Created by Bartosz Sepioło on 20.04.2017.
 */
angular.module('atcApp', ['ui.router', 'ui.bootstrap', 'uiRouterStyles', 'ngCookies', 'ngSanitize', 'infinite-scroll','ngMaterial', 'ngMessages', 'angularFileUpload', 'ngImgCrop', 'ngAnimate', 'ngMessages', 'ngScrollbars','vsGoogleAutocomplete', 'isteven-multi-select', 'appRoutes', 'MainCtrl', 'LoginCtrl', 'LoginCustomCtrl', 'RegisterCtrl', 'AppHeaderCtrl', 'UserProfileCtrl', 'UserProfileGalleryCtrl', 'UserProfileGalleryPreviewCtrl', 'UserDashboardCtrl', 'FriendsCtrl', 'FollowersCtrl', 'MessagesCtrl', 'BrowseCtrl', 'SuggestionsCtrl','RouletteCtrl',"FriendsListCtrl","UsersCtrl", "DictionariesCtrl"]);

