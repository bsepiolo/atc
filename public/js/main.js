/**
 * Created by Bartosz Sepioło on 25.04.2017.
 */
$(document).ready(function () {
    console.log("ready!");
    userAvatarCirclesAnimation();
});

function userAvatarCirclesAnimation() {

    $("html,body").mousemove(function (event) {
        var x = event.pageX / 80;
        var y = event.pageY / 80;
        $(".user-circles-element").css({
            "transform":"translateX("+x+"px)"
        });
    });
}
