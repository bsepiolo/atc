/**
 * Created by Bartosz Sepioło on 20.04.2017.
 */
angular.module('appRoutes', []).config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider
        .state('landing', {
            url: '',
            abstract: true,
            views: {
                'header': {
                    templateUrl: 'views/home/shared/header.html'
                },
                'footer': {
                    templateUrl: 'partials/layout/sections/footer.html'
                }
            }
        })
        .state('app', {
            url: '',
            abstract: true,
            views: {
                'header': {
                    templateUrl: 'views/app/shared/header.html'
                },
                'friendsSideBar': {
                    templateUrl: 'views/app/shared/friendsSideBar.html'
                },
                'footer': {
                    templateUrl: 'partials/layout/sections/footer.html'
                }
            }
        })
        .state('landing.home', {
            url: '/',
            views: {
                'container@': {
                    templateUrl: 'views/home/landingPage/landingPageView.html',
                    controller: 'landingPageController'
                }
            }
        })
        .state('landing.login', {
            url: '/login',
            views: {
                'container@': {
                    templateUrl: 'views/home/login/loginView.html',
                    controller: 'loginController'
                }
            }
        })
        .state('landing.verify-user', {
            url: '/verify-user',
            views: {
                'container@': {
                    templateUrl: 'views/home/loginCustom/loginCustomView.html',
                    controller: 'loginCustomController'
                }
            }
        })
        .state('landing.register', {
            url: '/register',
            views: {
                'container@': {
                    templateUrl: 'views/home/register/registerView.html',
                    controller: 'registerController'
                }
            }
        })
        .state('app.userDashboard', {
            url: '/dashboard',
            views: {
                'container@': {
                    templateUrl: 'views/app/userDashboard/userDashboardView.html',
                    controller: 'userDashboardController',
                    access: {restricted: true}
                },
                'tabs@': {
                    templateUrl: 'views/app/shared/tabs/tabpanel.html',
                  //  controller: 'userDashboardController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.browse', {
            url: '/browse',
            views: {
                'container@': {
                    templateUrl: 'views/app/browse/browseView.html',
                    controller: 'browseController',
                    access: {restricted: true}
                },
                'tabs@': {
                    templateUrl: 'views/app/shared/tabs/tabpanel.html',
                    //  controller: 'userDashboardController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.suggestions', {
            url: '/suggestions',
            views: {
                'container@': {
                    templateUrl: 'views/app/suggestions/suggestionsView.html',
                    controller: 'suggestionsController',
                    access: {restricted: true}
                },
                'tabs@': {
                    templateUrl: 'views/app/shared/tabs/tabpanel.html',
                    //  controller: 'userDashboardController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.users', {
            url: '/admin/users',
            views: {
                'container@': {
                    templateUrl: 'views/app/admin/users/usersView.html',
                    controller: 'usersController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.dictionaries', {
            url: '/admin/dictionaries',
            views: {
                'container@': {
                    templateUrl: 'views/app/admin/dictionaries/dictionariesView.html',
                    controller: 'dictionariesController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.roulette', {
            url: '/roulette',
            views: {
                'container@': {
                    templateUrl: 'views/app/roulette/rouletteView.html',
                    controller: 'rouletteController',
                    access: {restricted: true}
                },
                'tabs@': {
                    templateUrl: 'views/app/shared/tabs/tabpanel.html',
                    //  controller: 'userDashboardController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.userProfile', {
            url: '/profile/:id',
            views: {
                'container@': {
                    templateUrl: 'views/app/userProfile/userProfileView.html',
                    controller: 'userProfileController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.userProfileFriends', {
            url: '/profile/:id/following',
            views: {
                'container@': {
                    templateUrl: 'views/app/userProfile/friends/friendsView.html',
                    controller: 'friendsController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.userProfileFollowers', {
            url: '/profile/:id/followers',
            views: {
                'container@': {
                    templateUrl: 'views/app/userProfile/friends/followersView.html',
                    controller: 'followersController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.userProfileGallery', {
            url: '/profile/:id/gallery',
            views: {
                'container@': {
                    templateUrl: 'views/app/userProfileGallery/userProfileGalleryView.html',
                    controller: 'userProfileGalleryController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.userProfileGalleryPreview', {
            url: '/profile/:id/gallery/:image',
            views: {
                'container@': {
                    templateUrl: 'views/app/userProfileGallery/userProfileGalleryPreviewView.html',
                    controller: 'userProfileGalleryPreviewController',
                    access: {restricted: true}
                }
            }
        })
        .state('app.messages', {
            url: '/messages/:id',
            views: {
                'container@': {
                    templateUrl: 'views/app/userProfile/messages/messageView.html',
                    controller: 'messagesController',
                    access: {restricted: true}
                }
            }
        })
        .state('/test', {
            redirectTo: function () {
                window.location = 'http://localhost:3000/userDashboard';
            }
        })
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

}])
;