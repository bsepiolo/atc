/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */

angular.module('LoginCtrl', ['loginModule']).controller('loginController', function ($location, $scope,$rootScope, $cookies, loginService) {
    console.log("test ")
    loginService.get().then(function (response, error) {
        if(response){
            $location.path('/dashboard');

        }

        console.log(response)
        $cookies.remove('userData');

    })

    $scope.submit = function () {
        // loginService.getData(data, serverData).then(function (response, error) {
        //     if (error)
        //         console.error(error);
        //     console.log(serverData)
        //     // $scope.user = response.data;
        //     // console.log("user", $scope.user[0].name)
        //
        // })

        loginService.create($scope.login).then(function (response, error) {
            if (error)
                console.error(error);
            if (response.data.auth == true) {
                $location.path('/dashboard');
            }
            $rootScope.currentUserIdTmp = response.data.userData._id;

        })
    }
});