/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('loginModule', []).factory('loginService', ['$http', function($http) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/login');
        },


        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(userData) {
            return $http.post('/api/login/', userData);
        },

        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/login/' + id);
        }
    }

}]);