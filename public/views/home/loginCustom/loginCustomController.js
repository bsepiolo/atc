/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */

angular.module('LoginCustomCtrl', ['loginCustomModule']).controller('loginCustomController', function ($location, $scope, $rootScope, $cookies, loginCustomService) {
    console.log("test ")
    loginCustomService.get().then(function (response, error) {
       // response = {halo: "baza"};
       //  if(response){
       //      $location.path('/userDashboard');
       //
       //  }

    })
    if ($cookies.get('userData')) {

        $scope.login = {'email': $cookies.get('userData')};

        loginCustomService.sendUserData($scope.login).then(function (response, error) {
            if (error)
                console.error(error);
            if (response) {
                //$location.path('/userDashboard');
                console.log("accept", response)
                debugger;
                $scope.userData = response.data;

            }
        })
    }
    $scope.submit = function () {

        loginCustomService.create($scope.login).then(function (response, error) {
            if (error)
                console.error(error);
            if (response.data.auth == true) {
                $location.path('/dashboard');
            }
            $rootScope.currentUserIdTmp = response.data.userData._id;

        })
    }
});