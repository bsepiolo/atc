/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('MainCtrl', ['landingPageModule']).controller('landingPageController', function ($scope, $timeout, $location, $cookies, landingPageService) {

    $scope.tagline = 'To the moon and back!';
    $scope.labelClass = "input-unfocused";
    $scope.focusAnimation = function (value) {
        if (value == false && $scope.user.email === '') {
            $scope.labelClass = "input-unfocused";

        } else {
            $scope.labelClass = "input-focused";
        }
    }

    landingPageService.get().then(
        function(response) {
            debugger;
            $location.path('/dashboard');
            // success callback
            console.log("witaj",response)


        },
        function(response) {
            // failure callback,handle error here
            // response.data.message will be "This is an error!"

            console.log("error")
        }
    );
    $scope.submit = function () {
        $cookies.put('userData', $scope.user.email);

        landingPageService.create($scope.user).then(
            function(response) {
                console.log("jest")
                // success callback
                $location.path('/verify-user');


            },
            function(response) {
                // failure callback,handle error here
                // response.data.message will be "This is an error!"

                $location.path('/register');
            }
        );


    }
    $scope.counter = 0;
    var updateCounter = function () {
        $scope.counter++;
        if ($scope.counter > 9) {
            $scope.counter = 0;
        }
        $timeout(updateCounter, 1500);
    };
    updateCounter();
});