/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('registerModule', []).factory('registerService', ['$http', function($http) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/register');
        },


        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(userData) {
            return $http.post('/api/register', userData);
        },

        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        }
    }

}]);