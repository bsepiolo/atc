/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */

angular.module('RegisterCtrl', ['registerModule']).controller('registerController', function ($scope, $rootScope,$location, $cookies, registerService) {

    $scope.registerData = {'email': $cookies.get('userData')};
    $scope.readOnly = true;

    this.date = new Date();
    this.isOpen = false;
    registerService.get().then(function (response, error) {
        if (error)
            console.error(error);

        $scope.user = response.data;
      //  console.log("user", $scope.user[0].name)

    })

    $scope.enableInput = function(){
        $scope.readOnly = false;
        $cookies.remove('userData');
    }
    if($cookies.get('userData')==undefined){
        $scope.readOnly = false;
    }
    $scope.userGender = function(gender){
        if(gender=='male'){
            $scope.registerData.male = !$scope.registerData.male;
            $scope.registerData.female = false;
            $scope.registerData.gender = 'male';
            $scope.isMale= true;
            $scope.isFemale= false;

        }else{
            $scope.registerData.female = !$scope.registerData.female;
            $scope.registerData.male = false;
            $scope.registerData.gender = 'female';
            $scope.isMale= false;
            $scope.isFemale= true;

        }
    }
    $scope.submit = function () {
        registerService.create($scope.registerData).then(
            function(response) {
                // success callback
                debugger;
                $rootScope.currentUserIdTmp = response.data.result._id;

                $location.path('/dashboard');
            },
            function(response) {
                // failure callback,handle error here
                // response.data.message will be "This is an error!"
                $location.path('/register');
            }
        );
    }

});