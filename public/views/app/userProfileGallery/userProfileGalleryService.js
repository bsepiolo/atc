/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('userProfileGalleryModule', []).factory('userProfileGalleryService', ['$http','$stateParams', function($http,$stateParams) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/userProfile/'+$stateParams.id+"/gallery");
        },
        addComment : function(commentData) {
            return $http.post('/api/userGallery/'+$stateParams.id+"/picture/"+$stateParams.image+"/comment/add", commentData);
        },
        setAvatar : function(avatarData, imgId) {
            return $http.post('/api/userProfile/'+$stateParams.id+'/gallery/avatar/'+imgId, avatarData);
        },

        getImage : function() {
            return $http.get('/api/userGallery/'+$stateParams.id+"/picture/"+$stateParams.image);
        },
        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(userData) {
            return $http.post('/api/userProfile/'+$stateParams.id+"/gallery", userData);
        },
        uploadPhoto : function(userData) {
            return $http.post('/api/userProfile/'+$stateParams.id+'/gallery/files', userData);
        },
        logout : function() {
            return $http.get('/api/logout');
        },
        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        }
    }

}]);