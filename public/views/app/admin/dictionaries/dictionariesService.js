/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('dictionariesModule', []).factory('dictionariesService', ['$http', function($http) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/admin/dictionaries');
        },

        removeDictionary : function(dictionaryId) {
            return $http.delete("/api/admin/dictionaries/"+dictionaryId);
        },
        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        addHobby : function(userData) {
            return $http.post('/api/admin/dictionaries/hobby', userData);
        },
        addJob : function(userData) {
            return $http.post('/api/admin/dictionaries/job', userData);
        },
        addUniversity : function(userData) {
            return $http.post('/api/admin/dictionaries/university', userData);
        },
        addEyes : function(userData) {
            return $http.post('/api/admin/dictionaries/eyes', userData);
        },
        addFigure : function(userData) {
            return $http.post('/api/admin/dictionaries/figure', userData);
        },
        addHair : function(userData) {
            return $http.post('/api/admin/dictionaries/hair', userData);
        },
        addEducationLevel : function(userData) {
            return $http.post('/api/admin/dictionaries/education', userData);
        },
        logout : function() {
            return $http.get('/api/logout');
        },
        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/admin/dictionaries' + id);
        }
    }

}]);