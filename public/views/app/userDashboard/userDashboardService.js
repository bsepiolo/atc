/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('userDashboardModule', []).factory('userDashboardService', ['$http', function($http) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/userDashboard');
        },
        getPosts : function() {
            return $http.get('/api/userDashboard/posts');
        },

        getMorePosts : function() {
            return $http.get('/api/userDashboard/morePosts');
        },
        getMoreComments : function(postId,loadNumber) {
            return $http.get('/api/userDashboard/post/'+postId+'/comments/'+loadNumber);
        },
        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        addLike : function(userData) {
            return $http.post('/api/userDashboard/like/add', userData);
        },
        createPost : function(userData) {
            return $http.post('/api/userDashboard/posts/add', userData);
        },
        createComment : function(userData) {
            return $http.post('/api/userDashboard/posts/comments/add', userData);
        },
        removePost : function(id) {
            return $http.delete('/api/userDashboard/posts/'+id);
        },
        logout : function() {
            return $http.get('/api/logout');
        },
        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        }
    }

}]);