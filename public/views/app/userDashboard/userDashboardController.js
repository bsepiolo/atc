/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */

var app = angular.module('UserDashboardCtrl', ['userDashboardModule', 'ngSanitize']);

app.filter('parseUrlFilter', function () {
    return function (text, target, otherProp) {
        return text.replace(urlPattern, '<a target="' + target + '" href="$&">$&</a>') + " | " + otherProp;
    };
});
app.filter('parseYouTubeFilter', ['$sce', '$http', function ($sce, $http) {
    var urlPatternLink = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/gi;

    var urlPattern = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/gi;
    var urlPatternVimeo = /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/gi;
    var urlPatternDailyMotion = /^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/gi;
    return function (text, target, otherProp) {
        var linkTmp;
        var tmp

        if (text.match(urlPattern)) {
            linkTmp = text.match(urlPattern).toString().split("?v=")[text.match(urlPattern).toString().split("?v=").length - 1];

            tmp = text.replace(urlPattern, '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' + linkTmp + '" frameborder="0" allowfullscreen></iframe></div>')

        } else if (text.match(urlPatternVimeo)) {
            linkTmp = text.match(urlPatternVimeo).toString().split("/")[text.match(urlPatternVimeo).toString().split("/").length - 1];
            tmp = text.replace(urlPatternVimeo, '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/' + linkTmp + '" frameborder="0" allowfullscreen></iframe></div>')

        } else if (text.match(urlPatternDailyMotion)) {
            linkTmp = text.match(urlPatternDailyMotion).toString().split("/")[text.match(urlPatternDailyMotion).toString().split("/").length - 1];
            tmp = text.replace(urlPatternDailyMotion, '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="//www.dailymotion.com/embed/video/' + linkTmp + '" frameborder="0" allowfullscreen></iframe></div>')

        } else if (text.match(urlPatternLink)) {

            tmp = text.replace(urlPatternLink, '<a target="' + target + '" href="' + text.match(urlPatternLink) + '">' + text.match(urlPatternLink) + '</a>')
        }
        if (tmp) {
            return $sce.trustAs('html', tmp)
        } else {
            return text;
        }


    };
}]);

app.controller('userDashboardController', function ($scope, $cookies, $location, $sanitize, $timeout, $http, userDashboardService) {
    // $scope.post = [];
    $scope.props = {
        target: '_blank',
        otherProp: 'otherProperty'
    };

    $scope.removePost = function (postId) {
        userDashboardService.removePost(postId).then(
            function (response) {
                debugger;
                // if(response.data.isFriend == true){
                $scope.postsData= response.data
                // }
            },
            function (response) {
                debugger
            })
    }
    $scope.showType = 'text'
    $scope.postType = []
    $scope.statusAddActive = function (state) {
        if (state) {
            $scope.stateBoxClass = 'focused';
            $scope.backgroundStateClass = 'show';
        } else {
            $scope.stateBoxClass = '';
            $scope.backgroundStateClass = '';
        }
    }
    $scope.marginLeft = 20;
    $scope.slideRight  = function(){
        if($scope.marginLeft<20)
        $scope.marginLeft+=80;
    }
    $scope.slideLeft  = function(){
        if($scope.marginLeft>-140)
        $scope.marginLeft-=80;
    }
    $scope.addLike = function(postId,post){
        userDashboardService.addLike({postId: postId}).then(
            function (response) {
                console.log("ccccccccccccc", post.comments)
                debugger;

                post.likes= response.data.likes;
                $scope.isLiked= response.data.liked;
                //  console.log("ccccccccccccc", $scope.commentData)
            },
            function (response) {
            })
    }
    var busy = false;
    $scope.showLocation = function () {
        $scope.showType = 'location'
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position;
                    var latlon = position.coords.latitude + "," + position.coords.longitude;
$scope.locationAddress;
                    $scope.img_url = "https://maps.googleapis.com/maps/api/staticmap?center="
                        + latlon + "&zoom=14&size=500x200";
                    $http({
                        method: 'GET',
                        url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latlon+'&sensor=true'
                    }).then(function successCallback(response) {
                        debugger;
                        $scope.locationAddress = response.data.results[1].formatted_address;
                    }, function errorCallback(response) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
                    $scope.shareLocation = function(){
                        debugger;
                        userDashboardService.createPost([{postData: {latitude: position.coords.latitude, longitude: position.coords.longitude, locationName: $scope.locationAddress}}, {postType: $scope.showType}]).then(
                            function (response) {
                             //   debugger;
                                $scope.stateBoxClass = '';
                                $scope.backgroundStateClass = '';
                                $scope.post = undefined;
                                $scope.postsData = response.data;
                                if(response.data.type=="location"){
                                var wallLocationCoords = response.data.attachments.latitude +","+response.data.attachments.longitude
                                $scope.wallLocationUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + wallLocationCoords + "&zoom=14&size=500x200";

                                }
                                $scope.postType = []
                                $scope.showType = 'text'

                            },
                            function (response) {
                            })
                    }
                });
            });
        }
    }

    $scope.loadPosts = function () {

        userDashboardService.getMorePosts().then(
            function (response) {
                // $scope.postsData = []

                debugger;
                // success callback
                console.log("res", response)
                for (var i = 0; i < response.data.length; i++) {
                    $scope.postsData.push(response.data[i]);
                }
                debugger;
                $scope.postType = []
                //   $scope.$apply();
                //  $location.path("/userDashboard")

            },
            function (response) {
                // $location.path("/login")
            }
        );
    }
    userDashboardService.getPosts().then(
        function (response) {
            debugger;
            // success callback
            console.log("res", response)
            $scope.postsData = response.data;
            if(response.data.type == "location"){
                debugger;
                var wallLocationCoords = response.data.attachments.latitude +","+response.data.attachments.longitude;
                $scope.wallLocationUrl = "https://maps.googleapis.com/maps/api/staticmap?center="
                    + wallLocationCoords + "&zoom=14&size=500x200"
            }
           $scope.postType = []

            //  $location.path("/userDashboard")
        },
        function (response) {
            // $location.path("/login")
        }
    );

    $scope.loadComments = function (postId, post, loadNumber) {

        userDashboardService.getMoreComments(postId, loadNumber).then(
            function (response) {
                // $scope.postsData = []
                debugger;
                // debugger;
                // // success callback
                // console.log("res", response)
                for (var i = 0; i < response.data.length; i++) {
                    post.comments.push(response.data[i]);
                }
                debugger;
                $scope.postType = []
                // //   $scope.$apply();
                // //  $location.path("/userDashboard")

            },
            function (response) {
                // $location.path("/login")
            }
        );
    }

    $scope.addComment = function (postId, commentData, post) {
        userDashboardService.createComment([{postId: postId}, {commentData: commentData}]).then(
            function (response) {
                console.log("ccccccccccccc", post.comments)
                debugger;
                post.comments.unshift(response.data);
                //  console.log("ccccccccccccc", $scope.commentData)
            },
            function (response) {
            })
    }
    $scope.addPost = function (event) {
        if (event.target.id == "typeText") {
            $scope.postType.push("text");
        }
        console.log($scope.post)
        userDashboardService.createPost([{postData: $scope.post}, {postType: $scope.postType}]).then(
            function (response) {
                $scope.stateBoxClass = '';
                $scope.backgroundStateClass = '';
                $scope.post = undefined;
                $scope.postsData = response.data;
                $scope.postType = []
            },
            function (response) {
            })
    }

    userDashboardService.get().then(
        function (response) {
            // success callback
            console.log("res", response)
            $scope.userData = response.data;
            $scope.following = response.data.following.length;
            $scope.followers = response.data.followers.length;
            $scope.lastVisitors = response.data.lastVisitors;
            //  $location.path("/userDashboard")
        },
        function (response) {
            // $location.path("/login")
        }
    );
    $scope.logout = function () {
        userDashboardService.logout().then(
            function (response) {
                // success callback
                console.log("logout", response)
                $location.path("/")
            },
            function (response) {
                console.log("blad")
            }
        );
    }
    console.log("hej")

});