/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('userProfileModule', []).factory('userProfileService', ['$http','$stateParams', function($http,$stateParams) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/userProfile/'+$stateParams.id);
        },
        getCurrentUser : function() {
            return $http.get('/api/messages/');
        },
        getHobbies : function() {
            return $http.get('/api/hobbies');
        },
        getJobs : function() {
            return $http.get('/api/jobs');
        },
        getUniversityCourses : function() {
            return $http.get('/api/universityCourses');
        },
        getBasicDictionary : function() {
            return $http.get('/api/basicDictionary');
        },

        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(userData) {
            return $http.post('/api/userProfile/'+$stateParams.id, userData);
        },
        follow : function(userData) {
            return $http.post('/api/userProfile/'+$stateParams.id+"/friends", userData);
        },
        getAvatar : function() {
            return $http.get('/api/userProfile/'+$stateParams.id+'/gallery/avatar/');
        },
        unFollow : function(userData) {
            return $http.delete('/api/userProfile/'+$stateParams.id+"/friends", userData);
        },
        uploadPhoto : function(userData) {
            return $http.post('/api/userProfile/'+$stateParams.id+'/files', userData);
        },
        logout : function() {
            return $http.get('/api/logout');
        },
        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        }
    }

}]);