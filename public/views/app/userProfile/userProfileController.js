/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
'use strict';
var app = angular.module('UserProfileCtrl', ['userProfileModule']);
app.factory('socketio', ['$rootScope', function ($rootScope) {
    'use strict';

    var socket = io.connect('http://localhost:4000/chat');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        once: function (eventName, callback) {
            socket.once(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
}]);
app.controller('userProfileController', function ($scope,$rootScope, $cookies, $location, $stateParams, $window, $timeout, $mdDialog, $interval, $element,socketio, FileUploader, userProfileService) {
    $scope.theme = 'red';
    $scope.chatWith = function(userId){
        $rootScope.userToChatWith = userId;
    }
    $scope.isMyProfile = false;
    debugger;
    if($cookies.get("currentUser")===$stateParams.id){
        $scope.isMyProfile = true;
    }
    var isThemeRed = true;
    this.date = new Date();
    this.isOpen = false;
    $interval(function () {
        $scope.theme = isThemeRed ? 'blue' : 'red';

        isThemeRed = !isThemeRed;
    }, 2000);
    userProfileService.getCurrentUser().then(
        function (response) {

            debugger;
            $scope.currentUserId = response.data;
        },
        function (response) {
            console.log("blad")
        }
    );
    $scope.loadMessages = function(guestId){
        socketio.emit('load messages history', {
            toSendId: 5,
            currentUserId: $rootScope.currentUserIdTmp,
            to: guestId
        });
        $location.path("/messages/"+guestId)
    }
    $scope.showAdvanced = function (ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'dialog1.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            locals: {
                uploader: $scope.uploader,
                checkImage: controller
            }
        })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
    };

    function DialogController($scope, $mdDialog, uploader, checkImage) {

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };
        $scope.uploader = uploader;
        $scope.controller = checkImage;
    }


    $scope.isValidUser = true;


    var uploader = $scope.uploader = new FileUploader({
        url: '/api/userProfile/:id/files'
    });

    // FILTERS

    // a sync filter
    uploader.filters.push({
        name: 'syncFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            debugger;

            console.log('syncFilter');
            return this.queue.length < 10;
        }
    });

    // an async filter
    uploader.filters.push({
        name: 'asyncFilter',
        fn: function (item /*{File|FileLikeObject}*/, options, deferred) {
            debugger;
            console.log('asyncFilter');
            setTimeout(deferred.resolve, 1e3);
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
        $scope.setAvatar = function () {
            var index = uploader.getIndexOfItem(item);
            item.formData.push({title: "asdfdfasfasdfsafdsadfdfsa"});
        }

    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {


        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);
    var controller = $scope.controller = {
        isImage: function (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };
    // $scope.modernBrowsers = [
    //     {name: "Opera",	maker: "Opera Software",	ticked: true	},
    //     {name: "Internet Explorer",	maker: "Microsoft",	ticked: false	},
    //     {name: "Firefox",	maker: "Mozilla Foundation",	ticked: true	},
    //     {name: "Safari",	maker: "Apple",	ticked: false	},
    //     {name: "Chrome",	maker: "Google",	ticked: true	}
    // ];
    $scope.isValidUser = false;
    $scope.searchTerm;
    $scope.clearSearchTerm = function () {
        $scope.searchTerm = '';
    };
    // The md-select directive eats keydown events for some quick select
    // logic. Since we have a search input here, we don't need that logic.
    $element.find('input').on('keydown', function (ev) {
        ev.stopPropagation();
    });
    userProfileService.getBasicDictionary().then(
        function (response) {
            debugger;
            $scope.hair = response.data.hair[0];
            $scope.eyesColors = response.data.eyesColors[0];
            $scope.educationLevel = response.data.educationLevel[0];
            //$scope.hobbiesList = response.data;
            $scope.figure = response.data.figure[0];
        },
        function (response) {
            // $location.path("/login")
        })
    $scope.getHobbies = function () {
        userProfileService.getHobbies().then(
            function (response) {
                debugger;
                $scope.hobbiesList = response.data;
            },
            function (response) {
                // $location.path("/login")
            })
    }
    $scope.getJobs = function () {
        userProfileService.getJobs().then(
            function (response) {
                debugger;
                $scope.jobsList = response.data;
            },
            function (response) {
                // $location.path("/login")
            })
    }
    $scope.getUniversityCourses = function () {
        userProfileService.getUniversityCourses().then(
            function (response) {
                debugger;
                $scope.univeristyCoursesList = response.data;
            },
            function (response) {
                // $location.path("/login")
            })
    }
$scope.galleryLength = 4;
    userProfileService.get().then(
        function (response) {

            // success callback
            debugger;
            if (response.data.userId == $stateParams.id) {
                debugger;
                $scope.isValidUser = true;

                $scope.galleryLength = 3;

                debugger;
                $scope.submitBasic = function () {
                    // if ($scope.isValidUser != false) {
                    debugger;
                    userProfileService.create({
                        userData: $scope.userData,
                        placeOfBirth: $scope.placeOfBirth,
                        placeOfResidence: $scope.placeOfResidence
                    }).then(
                        function (response) {
                        },
                        function (response) {
                        })
                    // }
                }
                $scope.submitBody = function () {
                    debugger;
                    userProfileService.create({
                        userData: $scope.userData,
                        placeOfBirth: $scope.placeOfBirth,
                        placeOfResidence: $scope.placeOfResidence
                    }).then(
                        function (response) {
                            debugger;
                            //$scope.userData = response;
                        },
                        function (response) {
                            debugger
                        })
                }

                debugger;


                $scope.editSection = function (state) {
                    if (state == "basic") {
                        $scope.isActiveBasic = $scope.isActiveBasic ? false : true
                    } else if (state == "body") {
                        $scope.isActiveBody = $scope.isActiveBody ? false : true
                    } else if (state == "education") {
                        $scope.isActiveEducation = $scope.isActiveEducation ? false : true
                    }
                    else if (state == "career") {
                        $scope.isActiveCareer = $scope.isActiveCareer ? false : true
                    }
                }
            } else {
                $scope.isFriend = response.data.isFriend;
                $scope.addToFriends = function () {
                    userProfileService.follow().then(
                        function (response) {
                            debugger;
                            // if(response.data.isFriend == true){
                            $scope.isFriend = response.data.isFriend;
                            // }
                        },
                        function (response) {
                            debugger
                        })
                }
                $scope.removeFriend = function () {
                    userProfileService.unFollow().then(
                        function (response) {
                            debugger;
                            // if(response.data.isFriend == true){
                            $scope.isFriend = response.data.isFriend;
                            // }
                        },
                        function (response) {
                            debugger
                        })
                }
            }
            debugger;
            $scope.userData = angular.copy(response.data.result);
            $scope.placeOfBirth = angular.copy(response.data.result.placeOfBirth);
            $scope.placeOfResidence = angular.copy(response.data.result.placeOfResidence);
            $scope.comparisonPoints = response.data.currentPoints;
            $scope.following = response.data.result.following.length;
            $scope.followers = response.data.result.followers.length;
            $scope.gallery = response.data.result.gallery.reverse()
            $scope.chartPercentage = {'width': $scope.comparisonPoints * 10 + '%'};
            $scope.chartHeartMargin = {'left': $scope.comparisonPoints * 10 - 3 + '%'};
        },
        function (response) {
            // $location.path("/login")
        }
    );
    $scope.cancelSection = function (state) {
        userProfileService.get().then(
            function (response) {
                $scope.userData = angular.copy(response.data.result);
                if (state == "basic") {
                    $scope.isActiveBasic = $scope.isActiveBasic ? false : true
                } else if (state == "body") {
                    $scope.isActiveBody = $scope.isActiveBody ? false : true
                } else if (state == "education") {
                    $scope.isActiveEducation = $scope.isActiveEducation ? false : true
                }
                else if (state == "career") {
                    $scope.isActiveCareer = $scope.isActiveCareer ? false : true
                }
            },
            function (response) {
                // $location.path("/login")
            })
    }


    userProfileService.getAvatar().then(
        function (response) {
            debugger;
            $scope.userAvatar = response.data.currentName;

        },
        function (response) {
            // $location.path("/login")
        })


    $scope.logout = function () {
        userProfileService.logout().then(
            function (response) {
                // success callback
                console.log("logout", response)
                $location.path("/")
            },
            function (response) {
                console.log("blad")
            }
        );
    }
    console.log("hej")


}).run(function ($rootScope) {
    $rootScope.typeOf = function (value) {
        return typeof value;
    };
}).directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    }
});

