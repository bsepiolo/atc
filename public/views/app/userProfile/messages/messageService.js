/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('messagesModule', []).factory('messagesService', ['$http','$stateParams', function($http,$stateParams) {

    return {
        // call to get all nerds
        getCurrentUser : function() {
            return $http.get('/api/messages/*');
        },
        get : function() {
            return $http.get('/api/messages/'+$stateParams.id);
        },
        getFollowers : function() {
            return $http.get('/api/userProfile/'+$stateParams.id + '/followers');
        },
        unFollow : function(userData, friendId) {
            return $http.delete('/api/userProfile/'+friendId+"/friends", userData);
        },
        logout : function() {
            return $http.get('/api/logout');
        },
        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        }
    }

}]);