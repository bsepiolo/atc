/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
'use strict';
var app = angular.module('MessagesCtrl', ['messagesModule']);
app.factory('socketio', ['$rootScope', function ($rootScope) {
    'use strict';

    var socket = io.connect('http://localhost:4000/chat');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        once: function (eventName, callback) {
            socket.once(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
}]);
app.controller('messagesController', function ($scope, $rootScope, $cookies, $location, $stateParams, $window, socketio, messagesService) {
    $scope.currentUserId;
    $scope.messages = [];
    $scope.toSendId;
    //alert($rootScope.currentUserIdTmp)
    var conversationId;
    $scope.currentUserId = $rootScope.currentUserIdTmp;//$cookies.get('currentUser');
    $scope.config = {
        autoHideScrollbar: false,
        theme: 'minimal-dark',
        advanced:{
            updateOnContentResize: true
        },
       // setHeight: 200,
        scrollInertia: 0
    }

    // $scope.$on("$destroy", function(){
    //     $interval.cancel(myInterval);
    // });
    //$scope.currentUserId = $rootScope.currentUserIdTmp;
    // messagesService.getCurrentUser().then(
    //     function (response) {
    //
    //         debugger;
    //         $scope.currentUserId = response.data;
    //     },
    //     function (response) {
    //         console.log("blad")
    //     }
    // );

    // messagesService.get().then(
    //     function (response) {
    //         console.log("response", response)
    //         debugger;
    //         $scope.messages = response.data.messages.messages
    //         $scope.conversationList = response.data.conversations;
    //        // $scope.currentUserId = response.data;
    //     },
    //     function (response) {
    //         console.log("blad")
    //     }
    // );
    $scope.conversationList = [];
    socketio.emit("welcome", {test: "test"})
    $scope.joinRoom = function (conversation) {

        $scope.currentConversationId = conversation._id;
        $rootScope.userToChatWith = conversation.user1._id!==$scope.currentUserId?conversation.user1._id:conversation.user2._id;
        socketio.emit('leaveRoom', {
            to: conversation.user1._id!==$scope.currentUserId?conversation.user1._id:conversation.user2._id,
            toSendId: $scope.currentConversationId,
            currentUserId: $scope.currentUserId
        });

    }

    $scope.sendMessage = function () {
        //      console.log(data);
debugger;
        //$scope.messages.push({message: $scope.message.text});
        socketio.emit('message', {
            to: $rootScope.userToChatWith,
            toSendId: $scope.currentConversationId,
            from: $scope.currentUserId,
            message: $scope.message.text
        });
        $scope.message.text=''

        //socketio.emit("hello from header", {currentUserId: $cookies.get('currentUser')})
    }

    socketio.once('connection authorized', function (data) {


        socketio.emit("get conversation list", {currentUserId: $scope.currentUserId, to: $rootScope.userToChatWith})
        // socketio.emit('load history', {currentUserId: $scope.currentUserId, to: $rootScope.userToChatWith});

    })

    socketio.on('chatRoom messages', function (data) {

        //$scope.$apply(function() {
            $scope.messages = data.messages;
       // $scope.conversationList = data;
        debugger;
//debugger;
       // })
        for(var i =0;i<$scope.conversationList.length;i++){
            debugger;
            if($scope.conversationList[i].user1._id === $rootScope.userToChatWith || $scope.conversationList[i].user2._id === $rootScope.userToChatWith){
                $scope.currentConversationId =  $scope.conversationList[i]._id;

            }
            if($scope.conversationList[i].user1._id === $rootScope.userToChatWith){
                $scope.alienUser = $scope.conversationList[i].user1;
            }
            if($scope.conversationList[i].user2._id === $rootScope.userToChatWith){
                $scope.alienUser = $scope.conversationList[i].user2;
            }
        }
        // angular.forEach($scope.conversationList, function(value, key){
        //     debugger;
        //
        // });
        // $scope.conversationList = data.conversationList
    })

    socketio.once('chatRoom new', function (data) {
        $scope.conversationList[0].push(data)

    })

    socketio.once('chatRoom', function (data) {
    // w razie problemu zmienic z once na on
        console.log("data", data)

        $scope.conversationList = data;
debugger;
        for(var i =0;i<$scope.conversationList.length;i++){
            debugger;
            if($scope.conversationList[i].user1._id === $rootScope.userToChatWith || $scope.conversationList[i].user2._id === $rootScope.userToChatWith){
                $scope.currentConversationId =  $scope.conversationList[i]._id;

            }
            if($scope.conversationList[i].user1._id === $rootScope.userToChatWith){
                $scope.alienUser = $scope.conversationList[i].user1;
            }
            if($scope.conversationList[i].user2._id === $rootScope.userToChatWith){
                $scope.alienUser = $scope.conversationList[i].user2;
            }
        }

        //$scope.messages = data.result.messages;
        // if (data.length > 0) {
        //     debugger
        //     $scope.conversationList = data;
        //
        //     console.log("connected");
        //     var conUserId;
        //     for (var i = 0; i <= $scope.conversationList.length; i++) {
        //
        //         debugger;
        //         if($scope.conversationList[i]){
        //         if ($scope.conversationList[i].hasOwnProperty("user1")) {
        //             if ($scope.conversationList[i].user1._id == $rootScope.userToChatWith) {
        //                 conversationId = $scope.conversationList[i]._id;
        //                 socketio.emit('leaveRoom', {
        //                     to: $rootScope.userToChatWith,
        //                     toSendId: $scope.conversationList[i]._id,
        //                     currentUserId: $scope.currentUserId
        //                 });
        //
        //             }
        //         }
        //         if ($scope.conversationList[i].hasOwnProperty("user2")) {
        //             if ($scope.conversationList[i].user2._id == $rootScope.userToChatWith) {
        //                 conversationId = $scope.conversationList[i]._id;
        //                 socketio.emit('leaveRoom', {
        //                     to: $rootScope.userToChatWith,
        //                     toSendId: $scope.conversationList[i]._id,
        //                     currentUserId: $scope.currentUserId
        //                 });
        //             }
        //         }
        //     }
        //     }
        //
        //     $scope.toSendId = $scope.conversationList[0].user1._id !== $scope.currentUserId ? $scope.conversationList[0].user1._id : $scope.conversationList[0].user2._id;
        //     debugger;
        //     // conversationId = $scope.conversationList[0]._id;
        //     // if(stoploading == false) {
        // } else {
            $scope.toSendId = $rootScope.userToChatWith;
        //     debugger;
        // }

        socketio.emit('load messages history', {
            toSendId: $scope.currentConversationId,
            currentUserId: $scope.currentUserId,
            to: $scope.toSendId
        });
        //}
    });

    socketio.on('messageData', function (data) {
        //  debugger;


        if ($scope.messages.length > 0 && $scope.messages[$scope.messages.length - 1]._id != data._id) {
            $scope.messages.push(data);
            console.log(data)
        } else if (!$scope.messages.length) {
            $scope.messages.push(data);

        }
        console.log(data)

    })
    $scope.logout = function () {
        userProfileService.logout().then(
            function (response) {
                // success callback
                console.log("logout", response)
                $location.path("/")
            },
            function (response) {
                console.log("blad")
            }
        );
    }
    console.log("hej")


});