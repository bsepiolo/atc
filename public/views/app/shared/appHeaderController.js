/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */

var app = angular.module('AppHeaderCtrl', ['appHeaderModule']);

app.factory('socketio', ['$rootScope', function ($rootScope) {
    'use strict';

    var socket = io.connect('http://localhost:4000/chat');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        once: function (eventName, callback) {
            socket.once(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
}]);
app.controller('appHeaderController', function ($scope, $rootScope, $cookies, $location, appHeaderService, socketio, $timeout) {
    $scope.headerData = []
    $scope.userIdFromCookie = '';
    var conversationId;
    $scope.config = {
        autoHideScrollbar: false,
        theme: 'minimal-dark',
        advanced: {
            updateOnContentResize: true
        },
        // setHeight: 200,
        scrollInertia: 0,
        setWidth: 280
    }
    $scope.showAllMessages = function () {
        $rootScope.userToChatWith = undefined;
    }
    socketio.emit("welcome", {test: "test"})
    $scope.getClass = function (path) {
        return ($location.path().substr(0, path.length) === path) ? 'active' : '';
    }
    //console.log($scope.over)
    $scope.hideSearchResults = function () {
        debugger;
        $timeout(function () {
            $scope.search = {};
            $scope.search.data = '';
            $scope.hideNow = true;
        }, 200)
    }
    $scope.markAsReaded = function (notification) {
        console.log(notification.user._id)
        appHeaderService.markAsReaded({notificationId: notification.user._id}).then(
            function (response) {
                debugger;
                // success callback
                function checkReaded(message) {
                    return message.readed == "No";
                }

                $scope.unreadedNotificationsLength = response.data.followers.filter(checkReaded).length;
                $scope.notifications = response.data.followers.filter(checkReaded)
                console.log(response)
                //  $location.path("/userDashboard")
            },
            function (response) {
                // $location.path("/login")
            }
        );
    }
    appHeaderService.get().then(
        function (response) {
            debugger;
            // success callback
            function checkReaded(message) {
                return message.readed == "No";
            }

            $scope.unreadedNotificationsLength = response.data.followers.filter(checkReaded).length;
            $scope.notifications = response.data.followers.filter(checkReaded)
            debugger;
            $scope.headerData = response.data;
            $rootScope.currentUserIdTmp = response.data._id;
            console.log("ffffffffffffs", response)
            $cookies.put('currentUser', response.data._id);
            $scope.userIdFromCookie = $cookies.get("currentUser")
            //  $location.path("/userDashboard")
        },
        function (response) {
            // $location.path("/login")
        }
    );

    //alert('hey, myVar has changed!');

    // $scope.joinRoom = function (conversation) {
    //     //      console.log(data);
    //     debugger;
    //     // $scope.toSendId = conversation.user1._id !== $scope.headerData._id ? conversation.user1._id : conversation.user2._id;
    //     // conversationId = conversation._id;
    //     // console.log($scope.toSendId)
    //     // socketio.emit('leaveRoom', {to: $scope.toSendId, toSendId: conversationId, currentUserId: $scope.headerData._id, stopLoading: true});
    //     // socketio.emit('load history', {currentUserId: $scope.headerData._id, to: $scope.toSendId});
    //     //socketio.emit("stop loading", "stop");
    //
    //     $rootScope.userToChatWith = conversation.user1._id === $scope.headerData._id ? conversation.user2._id : conversation.user1._id;
    //     if(conversation.user1._id == $rootScope.userToChatWith){
    //         conversationId = conversation._id;
    //         socketio.emit('leaveRoom', {to: $rootScope.userToChatWith, toSendId: conversation._id, currentUserId: $scope.currentUserId});
    //
    //     }else if(conversation.user2._id == $rootScope.userToChatWith){
    //         conversationId = conversation._id;
    //         socketio.emit('leaveRoom', {to: $rootScope.userToChatWith, toSendId: conversation._id, currentUserId: $scope.currentUserId});
    //     }
    //
    // }
    debugger;
    $scope.search = {};
    $scope.searchData = function (event) {
        $scope.searchResults = ''
        $scope.hideNow = false;
        console.log($scope.search.data);
        //  $scope.search.data = thecourse.course_name;
        if ($scope.search.hasOwnProperty("data")) {
            if ($scope.search.data.length > 2 || event.keyCode === 8) {
                appHeaderService.search({searchData: $scope.search.data}).then(
                    function (response) {
                        debugger;
                        // success callback
                        if ($scope.search.data.length < 3) {
                            $scope.searchResults = ''
                        } else {
                            $scope.searchResults = response.data;
                        }
                        //  $location.path("/userDashboard")
                    },
                    function (response) {
                        // $location.path("/login")
                    }
                );
            }
        }
    }

    //only do this if $scope.course has not already been declared
    console.log($scope.search)
    $scope.$watch('userIdFromCookie', function () {
        $scope.joinRoom = function (user) {
            debugger;
            if (user.user1._id == $cookies.get('currentUser')) {
                $rootScope.userToChatWith = user.user2._id
            } else if (user.user2._id == $cookies.get('currentUser')) {
                $rootScope.userToChatWith = user.user1._id

            }
            // $rootScope.userToChatWith = userId;

        }
        socketio.emit("hello from header", {currentUserId: $rootScope.currentUserIdTmp})
    });


    socketio.on('header messages', function (data) {
        debugger;
        $scope.messages = data;
        if (data.length > 0) {
            function checkReaded(message) {
                if (message.messages.length > 0) {
                    return message.readed == "No" && message.messages[message.messages.length - 1].from._id !== $rootScope.currentUserIdTmp;
                }
            }

            $scope.unreadedMessages = data.filter(checkReaded).length;
        }
        debugger
    })

    socketio.on('header conversation', function (data) {
        debugger;
        socketio.emit("load header conversation", {currentUserId: $rootScope.currentUserIdTmp});
        debugger;
    })

    socketio.on('load last messages', function (data) {

        socketio.emit('newUser', $scope.currentUserId, "i");
    })

    // appHeaderService.getAvatar().then(
    //     function (response) {
    //         debugger;
    //         $scope.userAvatar = response.data.currentName;
    //
    //     },
    //     function (response) {
    //         // $location.path("/login")
    //     })
    $scope.logout = function () {
        appHeaderService.logout().then(
            function (response) {
                // success callback
                debugger
                console.log("logout", response)
                $cookies.remove('currentUser');

                $location.path("/")
            },
            function (response) {
                console.log("blad")
            }
        );
    }


});