/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */

var app = angular.module('FriendsListCtrl', ['friendsListModule']);

app.factory('socketio', ['$rootScope', function ($rootScope) {
    'use strict';

    var socket = io.connect('http://localhost:4000/chat');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        once: function (eventName, callback) {
            socket.once(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
}]);
app.controller('friendsListController', function ($scope, $rootScope, $cookies, $location, friendsListService, socketio, $timeout) {

    friendsListService.get().then(
        function (response) {
            debugger;
            $scope.friendsList = response.data;
        },
        function (response) {
            // $location.path("/login")
        }
    );



});