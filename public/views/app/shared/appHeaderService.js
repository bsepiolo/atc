/**
 * Created by Bartosz Sepioło on 05.05.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
/**
 * Created by Bartosz Sepioło on 22.04.2017.
 */
angular.module('appHeaderModule', []).factory('appHeaderService', ['$http', '$stateParams', function($http, $stateParams) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/appHeader');
        },
        markAsReaded : function(notificationId) {
            return $http.post('/api/appHeader/notification/readed', notificationId);
        },
        search : function(searchData) {
            return $http.post('/api/appHeader/search', searchData);
        },
        getAvatar : function() {
            return $http.get('/api/userProfile/'+$stateParams.id+'/gallery/avatar/');
        },

        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(userData) {
            return $http.post('/api/appHeader', userData);
        },
        logout : function() {
            return $http.get('/api/logout');
        },
        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/nerds/' + id);
        }
    }

}]);